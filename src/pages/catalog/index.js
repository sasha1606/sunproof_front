import React, {useState, useEffect} from "react";
import {Col, Container, Row} from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import {useTranslation} from "react-i18next";

import { catalogInfo } from '../../actions/catalog.action';

import { paginationLimitCatalog } from "../../const/pagination-limit";

import ProductCatalog from "../../components/product-catalog";
import Pagination from "../../components/pagination";
import ProductDropdown from "../../components/product-dropdown";
import PageTitle from "../../components/page-title";
import FreeEntry from "../../components/free-entry";

import './index.scss';


const Catalog = () => {
    const dispatch = useDispatch();

    const [currentPage, updateCurrentPage] = useState(1);
    const [selectedCategory, updateSelectedCategory] = useState(null);

    const [isFirstPage, updateIsFirstPage] = useState(false);
    const [isLastPage, updateIsLastPage] = useState(false);

    const { t } = useTranslation();

    const catalogStore = useSelector((store) => {

        return {
            allCatalog: store.catalogReducer.catalog.data,
            catalogPagination: store.catalogReducer.catalog.pagination,
            catalogCategory: store.catalogReducer.catalogCategory,
        }
    });

    useEffect(() => {
        // set document title
        document.title = t('document-title_catalog');
    }, []);

    useEffect(() => {
        if (!catalogStore.catalogCategory.length) {
            dispatch(catalogInfo.getCatalogCategory())
        }
    }, []);

    useEffect(() => {
        updateLastOrFirstPage();
    }, [catalogStore.allCatalog]);

    useEffect(() => {
            dispatch(catalogInfo.getCatalog(currentPage, paginationLimitCatalog, selectedCategory))
    }, [currentPage, selectedCategory]);

    const handlePageClick = (page) => {
        updateCurrentPage(page.selected + 1);
    };

    const filterByCategoryId = (categoryId) => {
        updateSelectedCategory(categoryId);
        updateCurrentPage(1);
    };

    const nextPage = () => {
        updateCurrentPage(currentPage + 1);
    };

    const prevPage = () => {
        updateCurrentPage(currentPage - 1);
    };

    const updateLastOrFirstPage = () => {
        updateIsLastPage(false);
        updateIsFirstPage(false);

        if (catalogStore.catalogPagination.pages === currentPage) {
            updateIsLastPage(true)
        }

        if (currentPage === 1) {
            updateIsFirstPage(true)
        }
    };

    return (
        <React.Fragment>
            <PageTitle title={t('menu_catalog')}></PageTitle>
            <div className='catalog__wrap'>
                <Container>
                    <Row>
                        <Col lg={3} md={5}>
                            <ProductDropdown
                                dropdownList={catalogStore.catalogCategory}
                                handelDropdownClick={filterByCategoryId}
                            />
                        </Col>
                        <Col lg={9} md={7}>
                            <ProductCatalog
                                uploadPrevProduct={prevPage}
                                uploadNextProduct={nextPage}

                                isLastPage={isLastPage}
                                isFirstPage={isFirstPage}

                                products={catalogStore.allCatalog}
                            />
                        </Col>
                    </Row>
                </Container>
            </div>
            {catalogStore.catalogPagination.pages > 1 &&
                <Pagination
                    pageCount={catalogStore.catalogPagination.pages}
                    currentPage={currentPage}
                    handlePageClick={handlePageClick}/>
            }
            <FreeEntry />
        </React.Fragment>
    )
};

export default Catalog;
