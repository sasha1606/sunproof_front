import React, {useEffect} from "react";
import {Col, Container, Row} from "react-bootstrap";
import {useTranslation} from "react-i18next";
import {useDispatch, useSelector} from "react-redux";

import {getAllComments} from '../../actions/comments.action';
import {getAllCustomers} from "../../actions/customers.action";

import PageTitle from "../../components/page-title";
import CustomersSlider from "../../components/customers-slider";
import SingleReviews from "../../components/single-reviews";
import SendReviews from "../../components/send-reviews";
import FreeEntry from "../../components/free-entry";

import customerMainImg from "../../assets/img/customer_main.png";

import './index.scss';

const Customers = () => {

    const dispatch = useDispatch();

    const { t } = useTranslation();

    const commentsStore = useSelector((store) => {
        return store.commentsReducer.comments.data;
    });

    const customersStore = useSelector((store) => {
        return store.customersReducer.customers;
    });

    useEffect(() => {
        // set document title
        document.title = t('document-title_customers');
    }, []);

    useEffect(() => {
        if (!commentsStore.length) {
            dispatch(getAllComments())
        }

        if (!customersStore.length) {
            dispatch(getAllCustomers())
        }
    }, []);

    return (
        <div className="customers">
            <PageTitle title={t('menu_customers')}></PageTitle>
            <Container>
                <div className='customers__img-wrap'>
                    <img src={customerMainImg} className='customers__img' alt='customers-img'/>
                </div>
                <h6 className='customers__title'>{t('customers_thank')}</h6>
                <div className='customers__thanks'>
                    <Row>
                        <Col lg={6} md={12}>
                            <p className='customers__thanks-content'>
                                {t('customers_info-1')}
                            </p>
                            <p className='customers__thanks-content'>
                                {t('customers_info-2')}
                            </p>
                        </Col>
                        <Col lg={6} md={12}>
                            <p className='customers__thanks-content'>
                                {t('customers_info-3')}
                            </p>
                        </Col>
                    </Row>
                </div>
                <div className='customers__slider'>
                    <CustomersSlider customers={customersStore}/>
                </div>
                <h6 className='customers__title'>{t('customers_reviews')}</h6>
                <div className='customers__reviews'>
                    <Container>
                        <Row>
                            {
                                commentsStore.map((comments) => {
                                    return (
                                        <Col key={comments.id} lg={4} md={6} xs={12}>
                                            <SingleReviews reviews={comments}/>
                                        </Col>
                                    )
                                })
                            }
                        </Row>
                    </Container>
                </div>
                <div className='customers__send-reviews'>
                    <SendReviews/>
                </div>
                <div className='customers__free-entry'>
                    <FreeEntry/>
                </div>
            </Container>
        </div>
    )
};

export default Customers;
