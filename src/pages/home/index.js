import React from 'react';
import {Link} from "react-router-dom";
import {Container} from "react-bootstrap";
import {useTranslation} from "react-i18next";

import btnAllIcon from "../../assets/img/icon/arrow_red.png";

import {baseLocaleUrl} from "../../const/baseLocaleUrl";

import HomeContact from "../../components/home-contact";
import HeroSlider from "../../components/hero-slider";
import HomeProduction from "../../components/home-production";
import AllProjects from "../../components/all-projects";
import FreeMeasurement from "../../components/free-measurement";
import HomeHowWeWorking from "../../components/home-how-we-working";

import './index.scss';

const Home = () => {
    const projectsLimit = 3;

    const { t } = useTranslation();

    return (
        <div className='hero-wrap'>
            <HeroSlider/>
            <HomeContact/>

            <HomeProduction/>
            <HomeHowWeWorking/>

            <div className='home-projects'>
                <AllProjects showAllProjectsBtn={false} withoutPagination={true} projectsLimit={projectsLimit}/>
                <Container>
                    <div className='text-center'>
                        <Link to={`${baseLocaleUrl}/projects`} className='button button_xxl button_transparent button_icon'>
                            {t('all-projects')}
                            <img src={btnAllIcon} alt='all'/>
                        </Link>
                    </div>
                </Container>
            </div>

            <FreeMeasurement/>
        </div>
    );
}


export default Home;
