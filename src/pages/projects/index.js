import React, { useEffect } from "react";
import {useTranslation} from "react-i18next";

import AllProjects from "../../components/all-projects";
import PageTitle from "../../components/page-title";
import FreeEntry from "../../components/free-entry";

// import mainProjectsImg from "../../assets/img/projects-main.png";

import './index.scss';

const Projects = () => {

    const { t } = useTranslation();

    useEffect(() => {
        // set document title
        document.title = t('document-title_projects');
    }, []);

    return (
        <React.Fragment>
            <PageTitle title={t('menu_projects')} />
            {/*<div className='text-center'>*/}
                {/*<img src={mainProjectsImg} className='project__img' alt='main-projects-img'/>*/}
            {/*</div>*/}
            <AllProjects  />
            <FreeEntry />
        </React.Fragment>
    )
};

export default Projects;
