import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Col, Container, Row } from "react-bootstrap";
import {useTranslation} from "react-i18next";

import { useDispatch, useSelector } from "react-redux";

// import { pagesInfo } from '../../actions/pages.action';

import FreeMeasurement from "../../components/free-measurement";

import {baseLocaleUrl} from "../../const/baseLocaleUrl";

import btnOpenIcon from "../../assets/img/icon/arrow_wthite.png";
import btnAllIcon from "../../assets/img/icon/arrow_red.png";
import systemsPopularImg from "../../assets/img/systems-popular-img.png";

import fullImgPath from "../../helpers/fullImgPath";
import switcherBackEndLanguage from "../../helpers/switcherBackEndLanguage";

import './index.scss';

const SystemsTypesSingle = ({match, history}) => {

    const dispatch = useDispatch();

    const { t } = useTranslation();

    const pagesStore = useSelector((store) => {
        return store.pagesReducer.pages
    });

    const [pageInfo, updatePageInfo] = useState({});

    // useEffect(() => {
    //     if(!pagesStore.length) {
    //         dispatch(pagesInfo.getAllPages())
    //     }
    // }, []);


    useEffect(() => {
        const categoryId = +match.params['categoryId'];
        if (pagesStore.length && !isNaN(match.params['categoryId'])) {
            const page = pagesStore.find( ({id}) => +id === categoryId);
            if(!page) {
                history.push(`${baseLocaleUrl}/`)
            }

            // set document title
            document.title = page[switcherBackEndLanguage('text_2')];

            updatePageInfo(page);
        }

    }, [pagesStore, match.params['categoryId']]);



    return (
        <React.Fragment>
            <Container fluid={true}>
                {/*start systems-types-single-hero*/}
                <div className='systems-types-hero'>
                    <div className='systems-types-hero__main-img'
                         style={{backgroundImage: `url(${fullImgPath(pageInfo.img_top)})`}}></div>
                    <div className='systems-types-hero__main-info'>
                        <div className='systems-types-hero__main-info-body'>
                            <h6 className='systems-types-hero__title'>{pageInfo[switcherBackEndLanguage('title_top')]}</h6>
                            <div className='systems-types-hero__main-text'
                                 dangerouslySetInnerHTML={{__html: pageInfo[switcherBackEndLanguage('text_top')]}}
                            ></div>
                        </div>
                        <div className='systems-types-hero__main-info-footer'>
                            <div>
                                <div className='systems-types-hero__main-name'>{pageInfo[switcherBackEndLanguage('category')]}</div>
                                <div className='systems-types-hero__main-price'>{ t('from') } {pageInfo[switcherBackEndLanguage('near_link_top')]}</div>
                            </div>
                            <Link className='button button_icon'
                                  to={{
                                      pathname: `${baseLocaleUrl}/catalog`,
                                      query: {catalogCategoryId: pageInfo.category_id}
                                  }}
                            >
                                {t('systems-types-single_open-catalog')}
                                <img src={btnOpenIcon} alt='icon'/>
                            </Link>
                        </div>
                    </div>
                </div>
                {/*end systems-types-single-hero*/}

                {/*start systems-popular-types*/}
                <div className='systems-popular-types'>
                    <h6 className='systems-popular-types__title'>{pageInfo[switcherBackEndLanguage('title_mid')]}</h6>
                    <Row>
                        <Col lg={6} md={12}>
                            <Row>
                                <Col md={6}>
                                    <div className='systems-popular-types__bg'
                                         style={{backgroundImage: `url(${fullImgPath(pageInfo.img_left_mid)})`}}></div>
                                </Col>
                                <Col md={6}>
                                    <div className='systems-popular-types__type-info'>
                                        <h6 className='systems-popular-types__type-title'>{pageInfo[switcherBackEndLanguage('title_mid_left')]}</h6>
                                        <div className='systems-popular-types__text'
                                             dangerouslySetInnerHTML={{__html: pageInfo[switcherBackEndLanguage('text_mid_left')]}}
                                        ></div>
                                        <div className='systems-popular-types__list'
                                             dangerouslySetInnerHTML={{__html: pageInfo[switcherBackEndLanguage('text_mid_left_bot')]}}
                                        ></div>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                        <Col lg={6} md={12}>
                            <Row>
                                <Col md={6}>
                                    <div className='systems-popular-types__bg'
                                         style={{backgroundImage: `url(${fullImgPath(pageInfo.img_right_mid)})`}}></div>
                                </Col>
                                <Col md={6}>
                                    <div className='systems-popular-types__type-info'>
                                        <h6 className='systems-popular-types__type-title'>{pageInfo[switcherBackEndLanguage('title_mid_right')]}</h6>
                                        <div className='systems-popular-types__text'
                                             dangerouslySetInnerHTML={{__html: pageInfo[switcherBackEndLanguage('text_mid_right')]}}
                                        ></div>
                                        <div className='systems-popular-types__list'
                                             dangerouslySetInnerHTML={{__html: pageInfo[switcherBackEndLanguage('text_mid_right_bot')]}}
                                        ></div>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </div>
                {/*end systems-popular-types*/}

                {/*start systems-types-single-img*/}
                <div className='systems-types-img'>
                    <img className='systems-types-img__img' src={systemsPopularImg} alt='systems-popular-img'/>
                </div>
                {/*end systems-types-single-img*/}

                {/*start systems-types-single-uniqueness*/}
                <div className='systems-types-uniqueness'>
                    <h6 className='systems-types-uniqueness__title'>{pageInfo[switcherBackEndLanguage('title_bot')]}</h6>
                    <Row>
                        <Col lg={3} md={6} xs={12}>
                            <div className='systems-types-uniqueness__content'
                                 dangerouslySetInnerHTML={{__html: pageInfo[switcherBackEndLanguage('text_not_1')]}}></div>
                        </Col>
                        <Col lg={3} md={6} xs={12}>
                            <div className='systems-types-uniqueness__content'
                                 dangerouslySetInnerHTML={{__html: pageInfo[switcherBackEndLanguage('text_not_2')]}}></div>
                        </Col>
                        <Col lg={3} md={6} xs={12}>
                            <div className='systems-types-uniqueness__content'
                                 dangerouslySetInnerHTML={{__html: pageInfo[switcherBackEndLanguage('text_not_3')]}}></div>
                        </Col>
                        <Col lg={3} md={6} xs={12}>
                            <div className='systems-types-uniqueness__content'
                                 dangerouslySetInnerHTML={{__html: pageInfo[switcherBackEndLanguage('text_not_4')]}}></div>
                        </Col>
                    </Row>
                </div>
                <div className='link-wrap-all'>
                    <Link
                        className='button button_xxl button_transparent button_icon'
                        to={{
                            pathname: `${baseLocaleUrl}/projects`,
                            query: {projectsCategoryId: pageInfo.project_category_id}
                        }}>
                        {t('all-projects')}
                        <img src={btnAllIcon} alt='all' />
                    </Link>

                </div>

                {/*end systems-types-single-uniqueness*/}
            </Container>
            <FreeMeasurement/>
        </React.Fragment>
    )
};

export default SystemsTypesSingle;
