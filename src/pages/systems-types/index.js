import React, { useState, useEffect } from "react";
import {useTranslation} from "react-i18next";
import { Col, Container, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";

import { systemsTypesInfo } from '../../actions/systems-types.action';

import { paginationLimitSystemsTypes } from "../../const/pagination-limit";

import Pagination from "../../components/pagination";
import ProductDropdown from "../../components/product-dropdown";
import PageTitle from "../../components/page-title";
import SystemsTypesCatalog from "../../components/systems-types-catalog";
import FreeEntry from "../../components/free-entry";

import './index.scss';

const SystemsTypes = () => {
    const dispatch = useDispatch();

    const { t } = useTranslation();

    const [currentPage, updateCurrentPage] = useState(1);
    const [selectedCategory, updateSelectedCategory] = useState(null);

    const [isFirstPage, updateIsFirstPage] = useState(false);
    const [isLastPage, updateIsLastPage] = useState(false);

    const systemsTypesStore = useSelector((store) => {
        return {
            allSystemsTypes: store.systemsTypesReducer.systemsTypes.data,
            systemsTypesPagination: store.systemsTypesReducer.systemsTypes.pagination,
            systemsTypesCategory: store.systemsTypesReducer.systemsTypesCategory,
        }
    });

    useEffect(() => {
        // set document title
        document.title = t('document-title_systems-types');
    }, []);

    useEffect(() => {
        if (!systemsTypesStore.systemsTypesCategory.length) {
            dispatch(systemsTypesInfo.getSystemsTypesCategory())
        }
    }, []);

    useEffect(() => {
        dispatch(systemsTypesInfo.getSystemsTypes(currentPage, paginationLimitSystemsTypes, selectedCategory))
    }, [currentPage, selectedCategory]);

    useEffect(() => {
        updateLastOrFirstPage();
    }, [systemsTypesStore.allSystemsTypes]);

    const handlePageClick = (page) => {
        updateCurrentPage(page.selected + 1);
    };

    const filterByCategoryId = (categoryId) => {
        updateSelectedCategory(categoryId);
        updateCurrentPage(1);
    };

    const nextPage = () => {
        updateCurrentPage(currentPage + 1);
    }

    const prevPage = () => {
        updateCurrentPage(currentPage - 1);
    }

    const updateLastOrFirstPage = () => {
        updateIsLastPage(false);
        updateIsFirstPage(false);

        if (systemsTypesStore.systemsTypesPagination.pages === currentPage) {
            updateIsLastPage(true)
        }

        if (currentPage === 1) {
            updateIsFirstPage(true)
        }
    };

    return (
        <React.Fragment>
            <PageTitle title={t('menu_systems-types')}></PageTitle>
            <div className='catalog__wrap'>
                <Container>
                    <Row>
                        <Col lg={3} md={5}>
                            <ProductDropdown
                                dropdownList={systemsTypesStore.systemsTypesCategory}
                                handelDropdownClick={filterByCategoryId}
                            />
                        </Col>
                        <Col lg={9} md={7}>
                            <SystemsTypesCatalog
                                uploadPrevProduct={prevPage}
                                uploadNextProduct={nextPage}
                                isLastPage={isLastPage}
                                isFirstPage={isFirstPage}
                                systemsTypes={systemsTypesStore.allSystemsTypes}
                            />
                        </Col>
                    </Row>
                </Container>
            </div>
            {systemsTypesStore.systemsTypesPagination.pages > 1 &&
            <Pagination
                currentPage={currentPage}
                pageCount={systemsTypesStore.systemsTypesPagination.pages}
                handlePageClick={handlePageClick}/>
            }
            <FreeEntry />
        </React.Fragment>
    )
};

export default SystemsTypes;
