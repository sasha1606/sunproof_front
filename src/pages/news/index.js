import React, {useEffect, useState} from "react";
import {Container} from "react-bootstrap";
import {useTranslation} from "react-i18next";
import { useDispatch, useSelector } from "react-redux";

import { getAllNews } from '../../actions/news.action';

// import newsImg from '../../assets/img/news_main.png';

import { paginationLimitNews } from "../../const/pagination-limit";

import PageTitle from "../../components/page-title";
import SingleNews from "../../components/single-news";
import Pagination from "../../components/pagination";

import './index.scss';

const News = () => {
    const dispatch = useDispatch();

    const { t } = useTranslation();

    const [currentPage, updateCurrentPage] = useState(1);

    useEffect(() => {
        // set document title
        document.title = t('document-title_news');
    }, []);

    useEffect(() => {
        dispatch(getAllNews(currentPage, paginationLimitNews))
    }, [currentPage]);

    const newsStore = useSelector((store) => {
        return {
            news: store.newsReducer.news.data,
            newsPagination: store.newsReducer.news.pagination,
        }
    });

    const handlePageClick = (page) => {
        updateCurrentPage(page.selected + 1);
    };

    return (
        <React.Fragment>
            <PageTitle title={t('menu_news')}/>
            <Container>
                {/*<div className='text-center'>*/}
                    {/*<img src={newsImg} className='news__img' alt='news-img'/>*/}
                {/*</div>*/}
                <div className='news-wrap'>
                    {newsStore.news.map( (news) => {
                        return <SingleNews news={news} key={news.id}/>
                    })}
                </div>
            </Container>

            {newsStore.newsPagination.pages > 1 &&
                <Pagination
                    currentPage={currentPage}
                    pageCount={newsStore.newsPagination.pages}
                    handlePageClick={handlePageClick}/>
            }
        </React.Fragment>
    )
};

export default News;
