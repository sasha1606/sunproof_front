import React, {useEffect} from 'react';
import { Col, Container, Row } from "react-bootstrap";
import {useTranslation} from "react-i18next";

import PageTitle from "../../components/page-title";
import Leaflet from "../../components/leaflet";
import ContactUsInfo from "../../components/contact-us-info";
import ContactUsForm from "../../components/contact-us-form";

import './index.scss';

const ContactUs = () => {

    const { t } = useTranslation();

    useEffect(() => {
        // set document title
        document.title = t('document-title_contact-us');
    }, []);

    return (
        <div className="">
            <PageTitle title={t('menu_contact-us')}></PageTitle>
            <Container>
                <div className='contact-us__info-wrap'>
                    <Row>
                        <Col md={6}>
                            <ContactUsInfo/>
                        </Col>
                        <Col md={6}>
                            <ContactUsForm/>
                        </Col>
                    </Row>
                </div>
                <Leaflet/>
            </Container>
        </div>
    );
}

export { ContactUs };
