import { apiPath } from '../../const/api';
import { http } from '../http';

const getCatalog = (page, pageLimit, categoryId) => {

    const httpParams = {
        pagination: {
            page,
            limit: pageLimit,
        }
    };

    if(categoryId) {
        httpParams['category_id'] = categoryId
    }

    return http.methodGet(apiPath.catalog, httpParams);
};

const getCatalogCategory = () => {
    return http.methodGet(apiPath.catalogCategory);
};

export const catalog = { getCatalog, getCatalogCategory };

