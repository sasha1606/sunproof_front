import { apiPath } from '../../const/api';
import { http } from '../http';

const getComments = () => {

    return http.methodGet(apiPath.comments);
}

const sendComments = (name, message) => {
    const formData = new FormData();
    formData.append('name', name);
    formData.append('comment', message);

    return http.methodPost(apiPath.sendComments, formData);
}

export const comments = { getComments, sendComments };

