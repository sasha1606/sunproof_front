import axios from 'axios';

import { baseUrl } from "../../const/api";

const methodGet = (path = '', params = {}, headers = {}) => {
    const headerProps = setHeaderProps(headers);
    return axios.get(
        `${baseUrl}/${path}`,
        {
            params,
            headerProps,
        }
    );
};

const methodPost = (path, params = {}, headers = {}) => {
    const headerProps = setHeaderProps(headers);

    return axios.post(
        `${baseUrl}/${path}`,
        params,
        {
            headerProps
        }
    );
};

const setHeaderProps = (headerProps) => {
    const contentType = {'Content-Type': 'application/json'};

    return Object.assign({}, contentType, headerProps);
};

export const http = {methodGet, methodPost};
