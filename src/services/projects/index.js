import { apiPath } from '../../const/api';
import { http } from '../http';

const getProjects = (page, pageLimit, categoryId) => {

    const httpParams = {
        pagination: {
            page,
            limit: pageLimit,
        }
    };

    if(categoryId) {
        httpParams['category_id'] = categoryId
    }

    return http.methodGet(apiPath.projects, httpParams);
};

const getProjectsCategory = () => {


    return http.methodGet(apiPath.projectsCategory);
};

export const projects = { getProjects, getProjectsCategory };

