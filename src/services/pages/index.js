import { apiPath } from '../../const/api';
import { http } from '../http';

const getAllPages = () => {

    return http.methodGet(apiPath.pages);
};

const getSinglePage = (pageId) => {

    return http.methodGet(apiPath.pages);
};

export const pages = { getAllPages, getSinglePage };

