import { apiPath } from '../../const/api';
import { http } from '../http';

const getNews = (page, pageLimit) => {
    const httpParams = {
        pagination: {
            page,
            limit: pageLimit,
        }
    };

    return http.methodGet(apiPath.news, httpParams);
};

export default getNews;

