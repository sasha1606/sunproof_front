import { apiPath } from '../../const/api';
import { http } from '../http';

const getCustomers = () => {
    return http.methodGet(apiPath.customers);
}

export default getCustomers;

