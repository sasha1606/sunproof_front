import { apiPath } from '../../const/api';
import { http } from '../http';

const getMainSliderInfo = () => {
    return http.methodGet(apiPath.mainSlider);
};

export const pageInfo = { getMainSliderInfo };

