import { apiPath } from '../../const/api';
import { http } from '../http';

const getSystemsTypes =(page, pageLimit, categoryId) => {

    const httpParams = {
        pagination: {
            page,
            limit: pageLimit,
        }
    };

    if(categoryId) {
        httpParams['category_id'] = categoryId
    }

    return http.methodGet(apiPath.systems, httpParams);
};

const getSystemsTypesCategory = () => {
    return http.methodGet(apiPath.systemCategory);
};

export const systemsTypes = { getSystemsTypes, getSystemsTypesCategory };

