import { apiPath } from '../../const/api';
import { http } from '../http';

const sendContactUs = (name, message, phone) => {

    const formData = new FormData();
    formData.append('name', name);
    formData.append('text', message);
    formData.append('phone', phone);

    return http.methodPost(apiPath.sendContactUs, formData);
};

const sendFreeEntry = (name, phone, email, message) => {

    const formData = new FormData();
    formData.append('name', name);
    formData.append('phone', phone);
    formData.append('email', email);
    formData.append('text', message);

    return http.methodPost(apiPath.sendSubscribe, formData);
};

const sendFreeMeasurement = (name, phone) => {

    const formData = new FormData();
    formData.append('name', name);
    formData.append('phone', phone);

    return http.methodPost(apiPath.sendClient, formData);
};

export const contactUs = { sendContactUs, sendFreeMeasurement, sendFreeEntry};

