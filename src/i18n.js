import i18n from 'i18next';
import { initReactI18next } from "react-i18next";

import translationRU from './assets/locales/ru.json';
import translationUA from './assets/locales/ua.json';

import { language } from './const/language';

import { selectedLanguageInUrl } from "./helpers/selectedLanguageInUrl";

i18n.on('languageChanged', function (lng) {

    const languageInUrl = selectedLanguageInUrl();
    if (languageInUrl === lng) return false;

    let newUrl = '';

    if (lng === language.ua) {
        newUrl = window.location.pathname.replace('/' + i18n.options.fallbackLng[0], '');
    } else {
        newUrl = `/${language.ru}${window.location.pathname}`;
    }

    window.location.replace(newUrl)
});


// the translations
const resources = {
    [language.ru]: {
        translation: translationRU
    },
    [language.ua]: {
        translation: translationUA
    },
};

i18n
    .use(initReactI18next)
    .init({
        resources,
        lng: selectedLanguageInUrl(),
        fallbackLng: [selectedLanguageInUrl()],
        interpolation: {
            escapeValue: false
        }
    });

export default i18n;