import React, {useState} from "react";
import {useTranslation} from "react-i18next";

import SuccessfullySentModal from "../modal-window/successfully-sent-modal";

import InputMask from "react-input-mask";

import {contactUs} from "../../services/contact-us";
import {formStatus} from "../../const/formStatus";

import { mapFormErrors } from "../../helpers/mapFormErrors";

import './index.scss';

const ContactUsForm = () => {

    const [name, updateName] = useState('');
    const [message, updateMessage] = useState('');
    const [phone, updatePhone] = useState('');

    const [formErrors, updateFormErrors] = useState({});

    const [isOpenSuccessfullySentModal, updateIsOpenSuccessfullySentModal] = useState(false);

    const { t } = useTranslation();

    const sendForm = (event) => {
        event.preventDefault();
        contactUs.sendContactUs(name, message, phone).then(
            (response) => {
                if (response.data.status === formStatus.error) {
                    const errors = mapFormErrors(response.data.error);
                    updateFormErrors(errors)
                } else {
                    commentsSuccessfullySent();
                }

            },
            (error) => {
                console.error(error);
                // REMOVE
                commentsSuccessfullySent();
            }
        )
    };

    const commentsSuccessfullySent = () => {
        updateName('');
        updateMessage('');
        updatePhone('');
        updateFormErrors({});
        updateIsOpenSuccessfullySentModal(true);
    };

    const closeSuccessfullySentModal = () => {
        updateIsOpenSuccessfullySentModal(false);
    };

    return (
        <React.Fragment>
            <h6 className='contact-us-form__title'>{t('write-to-us')}</h6>
            <form onSubmit={(event) => { sendForm(event) }} >
                <div className='form-group'>
                    <label htmlFor='contact-us-form-name' className=''>{t('form_name')}</label>
                    <input
                        placeholder={t('form_name')}
                        id='contact-us-form-name'
                        name='contact-us-form-name'
                        // required='required'
                        // minLength={3}
                        value={name}
                        onChange={e => updateName(e.target.value)}
                        className='form-control'/>
                    {formErrors.name &&
                        <span className='form-control-error'>{formErrors.name}</span>
                    }
                </div>
                <div className='form-group'>
                    <label htmlFor='contact-us-form-phone' className=''>{t('form_phone')}</label>
                    <InputMask
                        id='contact-us-form-phone'
                        name='contact-us-form-phone'
                        // required='required'
                        // minLength={8}
                        value={phone}
                        onChange={e => updatePhone(e.target.value)}
                        className='form-control'
                        alwaysShowMask={true}
                        mask="+3\8 999 999 99 99"/>
                    {formErrors.phone &&
                        <span className='form-control-error'>{formErrors.phone}</span>
                    }
                </div>
                <div className='form-group'>
                    <textarea
                        rows={3}
                        id='contact-us-message'
                        name='contact-us-message'
                        // required='required'
                        // minLength={3}
                        value={message}
                        placeholder={t('form_text')}
                        onChange={e => updateMessage(e.target.value)}
                        className='form-control'></textarea>
                    {formErrors.message &&
                        <span className='form-control-error'>{formErrors.message}</span>
                    }
                </div>
                <button className='button button_full-width' type='submit'>{t('send')}</button>
            </form>

            {isOpenSuccessfullySentModal &&
                <SuccessfullySentModal
                    message={'Ваше повідомлення доставлено!'}
                    closeActiveModal={closeSuccessfullySentModal}/>
            }
        </React.Fragment>
    )
}

export default ContactUsForm;
