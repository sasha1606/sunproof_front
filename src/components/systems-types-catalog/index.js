import React, {useState} from "react";
import {Col, Row} from "react-bootstrap";
import SliderProductModal from "../modal-window/slider-product-modal";

import './index.scss';
import fullImgPath from "../../helpers/fullImgPath";

const SystemsTypesCatalog = ({systemsTypes, uploadPrevProduct, uploadNextProduct, isLastPage, isFirstPage}) => {

    const [isOpenSliderProductModal, updateIsOpenSliderProductModal] = useState(false);
    const [clickedProduct, updateClickedProduct] = useState(null);

    const closeSliderProductModal = () => {
        updateClickedProduct(null);
        toggleSliderProductModal(false);
    };

    const toggleSliderProductModal = (isOpenModal) => {
        updateIsOpenSliderProductModal(isOpenModal)
    };

    const openSliderProductModal = (product) => {
        updateClickedProduct(product);
        toggleSliderProductModal(true);
    }

    return (
        <React.Fragment>
            <Row>
                {systemsTypes.map((systemsType) => {
                    return (
                        <Col key={systemsType.id} lg={3} md={6} xs={12}>
                            <div className='systems-type' onClick={(event) => openSliderProductModal(systemsType)}>
                                <div
                                    className='systems-type__img'
                                    style={{backgroundImage: `url(${fullImgPath(systemsType.image)})`}}
                                >
                                </div>
                            </div>
                        </Col>
                    )
                })
                }
            </Row>

            {isOpenSliderProductModal &&
                <SliderProductModal
                    uploadPrevProduct={uploadPrevProduct}
                    uploadNextProduct={uploadNextProduct}

                    isLastPage={isLastPage}
                    isFirstPage={isFirstPage}

                    products={systemsTypes}
                    selectedProducts={clickedProduct}

                    closeActiveModal={closeSliderProductModal}/>
            }
        </React.Fragment>
    )
};

export default SystemsTypesCatalog;
