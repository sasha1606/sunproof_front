import React, { useState } from "react";
import {useTranslation} from "react-i18next";

import {comments} from "../../services/comments";

import SuccessfullySentModal from "../modal-window/successfully-sent-modal";

import {formStatus} from "../../const/formStatus";

import { mapFormErrors } from "../../helpers/mapFormErrors";

import './index.scss';

const SendReviews = () => {
    const [name, updateName] = useState('');
    const [message, updateMessage] = useState('');
    const [formErrors, updateFormErrors] = useState({});

    const [isOpenSuccessfullySentModal, updateIsOpenSuccessfullySentModal] = useState(false);

    const { t } = useTranslation();

    const sendReview = (event) => {
        event.preventDefault();

        comments.sendComments(name, message).then(
            (response) => {
                if (response.data.status === formStatus.error) {
                    const errors = mapFormErrors(response.data.error);
                    updateFormErrors(errors)
                } else {
                    commentsSuccessfullySent();
                }
            },
            (error) => {
                console.error(error);
                // remove
                commentsSuccessfullySent();
            }
        )
    };

    const commentsSuccessfullySent = () => {
        updateName('');
        updateMessage('');
        updateFormErrors({});
        updateIsOpenSuccessfullySentModal(true);
    }

    const closeSuccessfullySentModal = () => {
        updateIsOpenSuccessfullySentModal(false);
    };

    return (
        <React.Fragment>
            <div className='send-reviews'>
                <h6 className='send-reviews__title'>{t('reviews_leave-feedback')}</h6>
                <form
                    className='send-reviews__form'
                    onSubmit={(event) => { sendReview(event) }}
                >
                    <div className='send-reviews__input-wrap'>
                        <div className='form-group'>
                            <label htmlFor='reviews-name' className=''>{t('form_name')}</label>
                            <input
                                placeholder={t('form_name')}
                                id='reviews-name'
                                name='reviews-name'
                                // required='required'
                                // minLength={3}
                                value={name}
                                onChange={e => updateName(e.target.value)}
                                className='form-control'/>
                            {formErrors.name &&
                                <span className='form-control-error'>{formErrors.name}</span>
                            }
                        </div>
                    </div>
                    <div className='form-group'>
                        <textarea
                            placeholder={t('reviews_enter-text')}
                            rows='4'
                            // required='required'
                            // minLength={3}
                            value={message}
                            name='reviews-message'
                            onChange={e => updateMessage(e.target.value)}
                            className='form-control'></textarea>
                        {formErrors.comment &&
                        <span className='form-control-error'>{formErrors.comment}</span>
                        }
                    </div>
                    <div className='text-center'>
                        <button
                            className='button'
                            type='submit'
                        >{t('publish')}</button>
                    </div>
                </form>
            </div>
            {isOpenSuccessfullySentModal &&
            <SuccessfullySentModal
                message={t('message-delivered')}
                closeActiveModal={closeSuccessfullySentModal}/>
            }
        </React.Fragment>
    )
};

export default SendReviews;
