import React from "react";
import { Container } from "react-bootstrap";

import './index.scss';

const PageTitle = ({ title }) => {
  return (
   <div className='page-title-container'>
       <Container>
           <h2 className='page-title-container__title'>{title}</h2>
       </Container>
   </div>
  )
};

export default PageTitle;
