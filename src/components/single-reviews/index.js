import React from "react";

import userAvatar from "../../assets/img/icon/reviews-avatar.png";

import './index.scss';

const SingleReviews = ({reviews}) => {
    return (
        <div className='single-reviews'>
            <div className='single-reviews__header'>
                <div className='single-reviews__user-info'>
                    <img className='single-reviews__avatar' src={userAvatar} alt='user'/>
                    <span className='single-reviews__name'>{reviews.name}</span>
                </div>
                <div className='single-reviews__date'>{reviews.date}</div>
            </div>
            <div className='single-reviews__content'>
                {reviews.comment}
            </div>
        </div>
    )
};

export default SingleReviews;
