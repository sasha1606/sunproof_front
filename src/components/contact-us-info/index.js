import React from "react";
import { useTranslation } from "react-i18next";

import contactTimeImg from '../../assets/img/contact/contact_time.png';
import contactTelImg from '../../assets/img/contact/contact_tel.png';
import contactEmailImg from '../../assets/img/contact/contact_email.png';

import './index.scss';


const ContactUsInfo = () => {

    const { t } = useTranslation();

    return (
        <React.Fragment>
            <div className='contact-us-info'>
                <img className='contact-us-info__icon' src={contactTimeImg} alt='contact-time'/>
                <h6 className='contact-us-info__title'>{t('work-schedule')}</h6>
                <p className='contact-us-info__content'>{t('work-schedule-days')} 08:00-20:00</p>
            </div>
            <div className='contact-us-info'>
                <img className='contact-us-info__icon' src={contactTelImg} alt='contact-tel'/>
                <h6 className='contact-us-info__title'>{t('contact-numbers')}</h6>
                <a href='tel:+380938990770' className='contact-us-info__content'>+380938990770</a>
                <br/>
                <a href='tel:+380688990770' className='contact-us-info__content'>+380688990770</a>
                <br/>
                <a href='tel:+380958990770' className='contact-us-info__content'>+380958990770</a>
            </div>
            <div className='contact-us-info'>
                <img className='contact-us-info__icon' src={contactEmailImg} alt='contact-email'/>
                <h6 className='contact-us-info__title'>Email</h6>
                <a href='mailto:Sunproofua@gmail.com' className='contact-us-info__content'>Sunproofua@gmail.com</a>
            </div>
        </React.Fragment>
    )
}

export default ContactUsInfo;
