import React, {useState, useEffect} from "react";
import {Col, Row} from "react-bootstrap";
import SliderProductModal from "../modal-window/slider-product-modal";

import fullImgPath from "../../helpers/fullImgPath";
import { productType } from "../../const/product-type";

import './index.scss';
import switcherBackEndLanguage from "../../helpers/switcherBackEndLanguage";

const ProductCatalog = ({products, uploadPrevProduct, uploadNextProduct, isLastPage, isFirstPage}) => {

    const [ isOpenSliderProductModal, updateIsOpenSliderProductModal ] = useState(false);
    const [ clickedProduct, updateClickedProduct ] = useState(null);

    const closeSliderProductModal = () =>  {
        updateClickedProduct(null);
        toggleSliderProductModal(false);
    };

    const toggleSliderProductModal = ( isOpenModal ) => {
        updateIsOpenSliderProductModal(isOpenModal)
    };

    const openSliderProductModal = (product) => {
        updateClickedProduct(product);
        toggleSliderProductModal(true);
    };

    return (
        <React.Fragment>
            <Row>
                {products.map((product) => {
                    return (
                        <Col key={product.id} lg={4} md={6} xs={12}>
                            <div className='single-product' onClick={() => openSliderProductModal(product)}>
                                <div
                                    className='single-product__img'
                                    style={{backgroundImage: `url(${fullImgPath(product.image)})`}}>

                                    {
                                        // isNew
                                        product.type === productType.isNew && <div className='single-product__new'>New</div>
                                    }

                                    {
                                        // isTop
                                        product.type === productType.isTop && <div className='single-product__top'>Top</div>
                                    }

                                    {
                                        // isSale
                                        product.type === productType.isSale && <div className='single-product__sale'>Sale</div>
                                    }
                                </div>
                                <div className='single-product__name'>{product[switcherBackEndLanguage('name')]}</div>
                                <div className='single-product__type'>{product[switcherBackEndLanguage('description')]}</div>
                            </div>
                        </Col>
                    )
                })
                }
            </Row>
            { isOpenSliderProductModal &&
                <SliderProductModal
                    products={products}
                    selectedProducts={clickedProduct}

                    uploadPrevProduct={uploadPrevProduct}
                    uploadNextProduct={uploadNextProduct}

                    isLastPage={isLastPage}
                    isFirstPage={isFirstPage}
                    closeActiveModal={closeSliderProductModal} />
            }

        </React.Fragment>
    )
};

export default ProductCatalog;
