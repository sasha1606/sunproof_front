import React, { useEffect, useState } from "react";
import { withRouter } from "react-router";

import './index.scss';
import switcherBackEndLanguage from "../../helpers/switcherBackEndLanguage";

const ProductDropdown = ({dropdownList, handelDropdownClick, location}) => {
    const [dropdown, updateDropdown] = useState([]);
    const [selectedItemId, updateSelectedItemId] = useState(null);
    const [selectedSubItemId, updateSelectedSubItemId] = useState(null);

    useEffect(() => {

        if (location.query && !isNaN(location.query.catalogCategoryId)) {
            itemClick(location.query.catalogCategoryId)
        }
    }, []);

    useEffect(() => {
        updateDropdown(dropdownList);
    }, [dropdownList]);

    const itemClick = (itemId) => {
        updateSelectedItemId(+itemId);
        updateSelectedSubItemId(null);
        handelDropdownClick(itemId)
    };

    const subItemClick = (event, subItemId) => {
        event.preventDefault();
        event.stopPropagation();

        updateSelectedSubItemId(+subItemId);
        handelDropdownClick(subItemId)
    };
    return (
        <React.Fragment>
            <ul className='product-dropdown'>
                {
                    dropdown.map((dropdown) => {
                        return (
                            <li
                                key={dropdown.id}
                                onClick={(event => itemClick(dropdown.id))}
                                className={
                                    `${dropdown.children.length ? 'product-dropdown__has-sub-item' : ''} 
                                     ${selectedItemId === +dropdown.id ? 'product-dropdown__has-sub-item_open' : ''}`
                                }
                            >
                                <span
                                    className={`product-dropdown__category-name ${selectedItemId === +dropdown.id ? 'product-dropdown__category-name_active' : ''}`}>
                                    {dropdown[switcherBackEndLanguage('name')]}
                                </span>
                                {
                                    dropdown.children.length > 0 &&
                                        <ul className='product-dropdown__sub-item-list'>
                                            {
                                                dropdown.children.map((childrenDropdown) => {
                                                    return (
                                                        <li
                                                            className={`product-dropdown__sub-item ${selectedSubItemId === +childrenDropdown.id ? 'product-dropdown__sub-item_active' : ''}`}
                                                            key={childrenDropdown.id}
                                                            onClick={(event => subItemClick(event, childrenDropdown.id))}
                                                        >{childrenDropdown[switcherBackEndLanguage('name')]}</li>
                                                    )
                                                })
                                            }
                                        </ul>
                                }
                            </li>
                        )
                    })
                }
            </ul>
        </React.Fragment>
    )
};

export default withRouter(ProductDropdown);
