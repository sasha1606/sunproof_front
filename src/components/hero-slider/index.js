import React, {useEffect} from 'react';
import {Link} from 'react-router-dom';
import Slider from "react-slick";
import {useTranslation} from "react-i18next";

import { useDispatch, useSelector } from "react-redux";

import { mainPgeInfo } from '../../actions/pageInfo.action';

import {baseLocaleUrl} from "../../const/baseLocaleUrl";

import fullImgPath from "../../helpers/fullImgPath";
import switcherBackEndLanguage from "../../helpers/switcherBackEndLanguage";

import logoWithe from "../../assets/img/logo/logo_withe.png";
import arrowSlides from "../../assets/img/icon/arrow_wthite.png";

import './index.scss';

function NextArrow(props) {
    const {currentSlide, slideCount, className, onClick} = props;

    return <div className='slick-arrow-wrap slick-arrow-wrap__next'><img className={className} onClick={onClick} src={arrowSlides} alt='nextArrow'/></div>;
}

function PrevArrow(props) {
    const {currentSlide, slideCount, className, onClick} = props;

    return <div className='slick-arrow-wrap slick-arrow-wrap__prev'><img className={className} onClick={onClick} src={arrowSlides} alt='prevArrow'/></div>;
}


const slickSettings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />
};

const HeroSlider = () => {

    const dispatch = useDispatch();

    const { t } = useTranslation();

    const mainSliderStore = useSelector((store) => {
        return store.pageInfoReducer.mainSlider;
    });

    useEffect(() => {
        if (!mainSliderStore.length) {
            dispatch(mainPgeInfo.getMainSlider())
        }
    }, []);

    return (
        <div className="hero-wrap">
            <Slider className='hero-slick-slider' {...slickSettings}>
                {mainSliderStore.map( slider => {
                    return (
                        <div key={slider.id} className='hero-slick-slider__item'>
                            <div className='hero-slick-slider__item-inner' style={{backgroundImage: `url(${fullImgPath(slider.image)})`}}>
                                <div className='hero-slick-slider__item-content'>
                                    <img className='hero-slick-slider__item-logo' src={logoWithe} alt='logo'/>
                                    <h2 className='hero-slick-slider__item-sub-title'>{slider[switcherBackEndLanguage('title')]}</h2>
                                    <h3 className='hero-slick-slider__item-title'>{slider[switcherBackEndLanguage('text')]}</h3>
                                    <div className='hero-slick-slider__item-price'>{t('from')} {slider.price} грн</div>
                                    <Link
                                        className='button button_icon'
                                        to={{
                                            pathname: `${baseLocaleUrl}/catalog`,
                                            query: {catalogCategoryId: slider.category_id}
                                        }}
                                    >
                                        {t('our-catalog')}
                                        <img src={arrowSlides} alt='btn-icon'/>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    )
                })}

            </Slider>
        </div>
    );
};

export default HeroSlider;
