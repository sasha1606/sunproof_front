import React, { useState } from 'react';
import {Col, Container, Row} from "react-bootstrap";
import {useTranslation} from "react-i18next";

// import AskQuestionModal from "../modal-window/ask-question-modal";
import AskQuestionWithSuccessfullyModal from "../modal-window/ask-question-with-successfully-modal";

import freeEntryImg from '../../assets/img/free-entry_2.png';
import freeEntryIcon from '../../assets/img/icon/letter.png';

import './index.scss';

const FreeEntry = () => {

    const [ isOpenQuestionWithSuccessfullyModal, updateIsOpenQuestionWithSuccessfullyModal ] = useState(false);

    const { t } = useTranslation();

    const closeAskQuestionModal = () =>  {
        toggleAskQuestionModal(false)
    };

    const toggleAskQuestionModal = ( isOpenModal ) => {
        updateIsOpenQuestionWithSuccessfullyModal(isOpenModal)
    };

    return (
        <React.Fragment>
            <Container>
                <div className='free-entry'>
                    <Row className=''>
                        <Col md={5}>
                            <div className='free-entry__img-wrap'>
                                <img className='free-entry__img' src={freeEntryImg} alt='free-entry'/>
                            </div>
                        </Col>
                        <Col md={7}>
                            <div className='text-center'>
                                <div className='free-entry__title'>{t('free-measurement')}</div>
                                <div className='free-entry__sub-title'>
                                    {t('free-info')}
                                </div>
                                <button onClick={() => toggleAskQuestionModal(true)} className='button button_icon button_big'>
                                    {t('record')}
                                    <img src={freeEntryIcon} alt='letter'/>
                                </button>
                            </div>
                        </Col>
                    </Row>
                </div>

            </Container>

            { isOpenQuestionWithSuccessfullyModal &&
                <AskQuestionWithSuccessfullyModal closeActiveModal={closeAskQuestionModal} />
            }
        </React.Fragment>
    )
};

export default FreeEntry
