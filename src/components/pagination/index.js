import React from "react";
import ReactPaginate from 'react-paginate';

// import arrowImg from '../../assets/img/icon/arrow_nav.png'

import './index.scss'

const Pagination = ({pageCount, handlePageClick, currentPage}) => {
    return (
        <div className='pagination-wrap'>
            <ReactPaginate
                previousLabel={'<'}
                nextLabel={'>'}
                breakLabel={'...'}
                breakClassName={'break-me'}
                pageCount={pageCount}
                forcePage={currentPage - 1}
                marginPagesDisplayed={2}
                pageRangeDisplayed={5}
                onPageChange={handlePageClick}
                containerClassName={'pagination'}
                subContainerClassName={'pages pagination'}
                activeClassName={'active'}
            />
        </div>
    )
};

export default Pagination;
