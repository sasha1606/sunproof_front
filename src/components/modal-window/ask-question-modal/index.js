import React, {useState} from "react";
import InputMask from "react-input-mask";
import { useTranslation } from "react-i18next";

import closeModalImg from '../../../assets/img/icon/close_modal.png';

import {contactUs} from "../../../services/contact-us";
import {formStatus} from "../../../const/formStatus";

import { mapFormErrors } from "../../../helpers/mapFormErrors";

import '../index.scss';


const AskQuestionModal = ({closeActiveModal, successfullyCallBack}) => {

    const [name, updateName] = useState('');
    const [phone, updatePhone] = useState('');
    const [email, updateEmail] = useState('');
    const [message, updateMessage] = useState('');

    const [formErrors, updateFormErrors] = useState({});

    const { t } = useTranslation();

    const sendForm = (event) => {
        event.preventDefault();
        contactUs.sendFreeEntry(name, phone, email, message).then(
            (response) => {
                if (response.data.status === formStatus.error) {
                    const errors = mapFormErrors(response.data.error);
                    updateFormErrors(errors)
                } else {
                    commentsSuccessfullySent();
                    successfullyCallBack()
                }
            },
            (error) => {
                console.error(error);
                // remove
                commentsSuccessfullySent();
            }
        )
    };

    const closeModal = () => {
        closeActiveModal();
    };


    const commentsSuccessfullySent = () => {
        updateName('');
        updatePhone('');
        updateEmail('');
        updateMessage('');

        updateFormErrors({});

        // closeModal();
    };

    return (
        <div className='modal-window'>
            <div className='modal-window__overlay' onClick={closeModal}></div>
            <div className='modal-window__content'>
                <div className='modal-window__close' onClick={closeModal}>
                    <img src={closeModalImg} alt='close'/>
                </div>
                <div className='modal-window__content-inner'>
                    <h6 className='modal-window__title'>{t('write-to-us')}</h6>
                    <form className='ask-question__form' onSubmit={(event) => {
                        sendForm(event)
                    }}>
                        <div className='form-group'>
                            <label htmlFor='ask-question-form-name' className=''>{t('form_name')}</label>
                            <input
                                className='form-control'
                                id='ask-question-form-name'
                                type='text'
                                placeholder={t('form_name')}
                                // required='required'
                                // minLength={3}
                                value={name}
                                name='ask-question-form-name'
                                onChange={e => updateName(e.target.value)}/>
                            {formErrors.name &&
                            <span className='form-control-error'>{formErrors.name}</span>
                            }
                        </div>
                        <div className='form-group'>
                            <label htmlFor='ask-question-form-phone' className=''>{t('form_phone')}</label>
                            <InputMask
                                className='form-control'
                                id='ask-question-form-phone'
                                name='ask-question-form-phone'
                                mask="+3\8 999 999 99 99"
                                // required='required'
                                // minLength={3}
                                alwaysShowMask={true}
                                value={phone}
                                onChange={e => updatePhone(e.target.value)}/>
                            {formErrors.phone &&
                            <span className='form-control-error'>{formErrors.phone}</span>
                            }
                        </div>
                        <div className='form-group'>
                            <label htmlFor='ask-question-form-email' className=''>{t('form_your-email')}</label>
                            <input
                                className='form-control'
                                id='ask-question-form-email'
                                name='ask-question-form-email'
                                type='email'
                                placeholder={t('form_your-email')}
                                // required='required'
                                // minLength={3}
                                value={email}
                                onChange={e => updateEmail(e.target.value)}/>
                            {formErrors.email &&
                            <span className='form-control-error'>{formErrors.email}</span>
                            }
                        </div>
                        <div className='form-group'>
                    <textarea
                        className='form-control'
                        placeholder={t('form_enter-message')}
                        rows='5'
                        name='ask-question-form-message'
                        value={message}
                        onChange={e => updateMessage(e.target.value)}></textarea>
                            {formErrors.text &&
                            <span className='form-control-error'>{formErrors.text}</span>
                            }
                        </div>
                        <button type="submit" className='button button_full-width'>
                            {t('send')}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    )
};

export default AskQuestionModal;
