import React, {useEffect, useState} from "react";

import { productType } from "../../../const/product-type";

import navImg from '../../../assets/img/icon/arrow_balck.png';
import closeModalImg from '../../../assets/img/icon/close_modal.png';

import fullImgPath from "../../../helpers/fullImgPath";
import switcherBackEndLanguage from "../../../helpers/switcherBackEndLanguage";

import '../index.scss';
import './index.scss';

const SliderProductModal = ({closeActiveModal, products, selectedProducts, uploadNextProduct, uploadPrevProduct, isLastPage, isFirstPage}) => {

    const [activeItem, updateActiveItem] = useState(null);
    const [selectedIndex, updateSelectedIndex] = useState(null);

    const [isUploadNext, updateIsUploadNext] = useState(false);
    const [isUploadPrev, updateIsUploadPrev] = useState(false);

    useEffect(() => {
        if (products && products.length && selectedProducts) {
            updateActiveItem(selectedProducts);
            matchInitSelectedIndex()
        }
    }, [selectedProducts]);

    useEffect(() => {
        updateUploadedSelectedItem()
    }, [products]);

    const closeModal = () => {
        closeActiveModal();
    };

    const matchInitSelectedIndex = () => {
        updateSelectedIndex(products.indexOf(selectedProducts))
    };

    const nextItem = () => {
        const nextIndex = selectedIndex + 1;

        resetUpdatedFlags();

        if (selectedIndex !== products.length - 1) {
            updateActiveItem(products[nextIndex]);
            updateSelectedIndex(nextIndex);
        } else {
            uploadNextProduct();
            updateIsUploadNext(true);
        }

    };

    const prevItem = () => {
        const prevIndex = selectedIndex - 1;

        resetUpdatedFlags();

        if (selectedIndex !== 0) {
            updateActiveItem(products[prevIndex]);
            updateSelectedIndex(prevIndex);
        } else {
            uploadPrevProduct();
            updateIsUploadPrev(true);
        }

    };

    const resetUpdatedFlags = () => {
        updateIsUploadPrev(false);
        updateIsUploadNext(false);
    };

    const updateUploadedSelectedItem = () => {
        if (isUploadNext) {
            updateActiveItem(products[0]);
            updateSelectedIndex(0)
        } else if (isUploadPrev) {
            const lastProductsIndex = products.length - 1;
            updateActiveItem(products[lastProductsIndex]);
            updateSelectedIndex(lastProductsIndex)
        }
    };


    const displayProductType = () => {
        if (activeItem.hasOwnProperty('type')) {
            let htmlType;

            switch (activeItem.type) {
                case productType.isNew:
                    htmlType = <div className='slider-product-modal__new'>New</div>;

                    break;

                case productType.isTop:
                    htmlType = <div className='slider-product-modal__top'>Top</div>;

                    break;

                case productType.isSale:
                    htmlType = <div className='slider-product-modal__sale'>Sale</div>;

                    break;

                default:
                    htmlType = null;
            }

            return htmlType;
        }

        return null;
    };


    return (
        <React.Fragment>
            {activeItem && <div className='modal-window'>
                <div className='modal-window__overlay' onClick={closeModal}></div>
                <div className='modal-window__content'>
                    <div className='modal-window__close' onClick={closeModal}>
                        <img src={closeModalImg} alt='close'/>
                    </div>
                    <div className='modal-window__content-inner'>

                        {
                            // isNew
                            activeItem.type === productType.isNew &&
                            <div className='slider-product-modal__new'>New</div>
                        }

                        {
                            // isTop
                            activeItem.type === productType.isTop &&
                            <div className='slider-product-modal__top'>Top</div>
                        }

                        {
                            // isSale
                            activeItem.type === productType.isSale &&
                            <div className='slider-product-modal__sale'>Sale</div>
                        }

                        <div className='slider-product-modal'>
                            {activeItem[switcherBackEndLanguage('title')] &&
                            <h6 className='slider-product-modal__title'>{activeItem[switcherBackEndLanguage('title')]}</h6>}
                            {isFirstPage && selectedIndex === 0 ? '' :
                                <span onClick={prevItem}
                                      className='slider-product-modal__nav slider-product-modal__nav_prev'>
                                    <img src={navImg} alt='nav'/>
                                </span>
                            }
                            {isLastPage && selectedIndex === products.length - 1 ? '' :
                                <span onClick={nextItem}
                                      className='slider-product-modal__nav slider-product-modal__nav_next'>
                                    <img src={navImg} alt='nav'/>
                                </span>
                            }
                            <div className='slider-product-modal__img-wrap'>
                                <img src={fullImgPath(activeItem.image)} alt='product'/>
                            </div>
                            <div className='slider-product-modal__footer'>
                                <div className='slider-product-modal__name'>{activeItem[switcherBackEndLanguage('name')]}</div>
                                {activeItem[switcherBackEndLanguage('description')] &&
                                <div className='slider-product-modal__info'>{activeItem[switcherBackEndLanguage('description')]}</div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            }
        </React.Fragment>
    )
};

export default SliderProductModal;
