import React from "react";
import { useTranslation } from "react-i18next";

import successfullySentImg from '../../../assets/img/successfully-sent.png';
import closeModalImg from '../../../assets/img/icon/close_modal.png';

import '../index.scss';

const SuccessfullySentModal = ({message, closeActiveModal}) => {

    const { t } = useTranslation();

    const closeModal = () => {
        closeActiveModal();
    };

    return (
        <div className='modal-window'>
            <div className='modal-window__overlay' onClick={closeModal}></div>
            <div className='modal-window__content'>
                <div className='modal-window__close' onClick={closeModal}>
                    <img src={closeModalImg} alt='close'/>
                </div>
                <div className='modal-window__content-inner modal-window__content-inner_xs'>
                    <div className='text-center'>
                        <img className='successfully-sent__img' src={successfullySentImg} alt='successfully-sent'/>
                    </div>
                    <p className='successfully-sent__message'>{message}</p>
                    <button type="button" onClick={closeModal} className='button button_full-width'>{t('return-to-site')}
                    </button>
                </div>
            </div>
        </div>
    )
};

export default SuccessfullySentModal;
