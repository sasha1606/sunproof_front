import React, { useState } from "react";
import { useTranslation } from "react-i18next";

import AskQuestionModal from "../ask-question-modal";
import SuccessfullySentModal from "../successfully-sent-modal";


const AskQuestionWithSuccessfullyModal = ( {closeActiveModal} ) => {

    const [isOpenSuccessfullySentModal, updateIsOpenSuccessfullySentModal] = useState(false);
    const [isOpenAskQuestionModal, updateIsOpenAskQuestionModal] = useState(true);

    const { t } = useTranslation();

    const closeCurrentModal = () => {
        closeActiveModal();
        // updateIsOpenSuccessfullySentModal(false);
    };

    const openSuccessfullySentModal = () => {
        updateIsOpenSuccessfullySentModal(true);
        updateIsOpenAskQuestionModal(false);
    };

    return (
        <React.Fragment>
            {isOpenAskQuestionModal &&
                <AskQuestionModal
                successfullyCallBack={openSuccessfullySentModal}
                closeActiveModal={closeCurrentModal}/>
            }
            {isOpenSuccessfullySentModal &&
                <SuccessfullySentModal
                    message={t('message-delivered')}
                    closeActiveModal={closeCurrentModal}/>
            }
        </React.Fragment>
    )
};

export default AskQuestionWithSuccessfullyModal;
