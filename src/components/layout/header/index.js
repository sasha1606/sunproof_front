import React, {useEffect, useState} from 'react';
import {NavLink} from 'react-router-dom';
import {withRouter} from "react-router";
import { useTranslation } from "react-i18next";
import ReactPixel from 'react-facebook-pixel';
import {useDispatch, useSelector} from "react-redux";

import {pagesInfo} from '../../../actions/pages.action';

import HeaderSocials from './header-socials';

import {language} from "../../../const/language";
import { facebookPixelId } from '../../../const/facebookPixelId';
import {baseLocaleUrl} from "../../../const/baseLocaleUrl";

import switcherBackEndLanguage from "../../../helpers/switcherBackEndLanguage";

import logo from '../../../assets/img/logo/logo.png';

import i18n from '../../../i18n'

import './index.scss';

const Header = ({match }) => {

    const [isOpenMobMenu, updateIsOpenMobMenu] = useState(false);
    const [isOpenMobSubMenu, updateIsOpenSubMenu] = useState(false);

    const dispatch = useDispatch();

    const { t } = useTranslation();

    useEffect(() => {

        initFacebookPixel();

        if (!pagesStore.length) {
            dispatch(pagesInfo.getAllPages())
        }
    }, []);

    useEffect(() => {
        updateIsOpenMobMenu(false);
        updateIsOpenSubMenu(false);
    }, [match]);

    const pagesStore = useSelector((store) => {
        return store.pagesReducer.pages
    });

    const toggleMobMenu = () => {
        const newStateMobMenu = !isOpenMobMenu;
        updateIsOpenMobMenu(newStateMobMenu);
    };

    const toggleMobSubMenu = () => {
        const newStateMobSubMenu = !isOpenMobSubMenu;
        updateIsOpenSubMenu(newStateMobSubMenu);
    };

    const initFacebookPixel = () => {
        // init facebook pixel
        ReactPixel.init(facebookPixelId);
        ReactPixel.pageView();
    };

    const changeLanguage = (lng) => {
        i18n.changeLanguage(lng);
    }


    return (
        <header className={`top-navbar ${isOpenMobMenu ? 'top-navbar_mob-open' : ''}`}>
            <div className='top-navbar__info'>
                <div className='top-navbar__socials_desktop'>
                    <HeaderSocials />
                </div>
                <div className='top-navbar__info-inner'>
                    <a href='tel:+380938990770' className='top-navbar__info-item'>+380938990770</a>
                    <a href='tel:+380688990770' className='top-navbar__info-item'>+380688990770</a>
                    <a href='tel:+380958990770' className='top-navbar__info-item'>+380958990770</a>
                </div>
            </div>
            <div className='top-navbar__main'>
                <NavLink to={`${baseLocaleUrl}/`}>
                    <img className='top-navbar__logo' src={logo} alt='logo'/>
                </NavLink>
                <div className='top-navbar__socials_mobile'>
                    <HeaderSocials />
                </div>
                <div onClick={toggleMobMenu} className='top-navbar__mob-toggle'>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <div className="top-navbar__navigation">
                    <ul className='top-navbar-menu'>
                        <li>
                            <NavLink exact activeClassName='is-active' to={`${baseLocaleUrl}/`}>{t('menu_home')}</NavLink>
                        </li>
                        <li className='top-navbar-menu__sub-menu'>
                            <span className='top-navbar-menu__sub-menu-name'>{t('menu_product')}</span>
                            <ul className='top-navbar-menu__sub-menu-list'>
                                {
                                    pagesStore.map((page) => {
                                        return (
                                            <li key={page.id}>
                                                <NavLink
                                                    activeClassName='is-active'
                                                    to={`${baseLocaleUrl}/systems-types/${page.id}`}>
                                                    {page[switcherBackEndLanguage('category')]}
                                                </NavLink>
                                            </li>
                                        )
                                    })
                                }
                            </ul>
                        </li>
                        <li className={`top-navbar-menu__sub-menu_mob ${isOpenMobSubMenu ? 'top-navbar-menu__sub-menu_mob_open' : ''}`}>
                            <span onClick={toggleMobSubMenu}
                                  className='top-navbar-menu__sub-menu-name_mob'>{t('menu_product')}</span>
                            <ul className='top-navbar-menu__sub-menu-list_mob'>
                                {
                                    pagesStore.map((page) => {
                                        return (
                                            <li key={page.id}>
                                                <NavLink
                                                    activeClassName='is-active'
                                                    to={`${baseLocaleUrl}/systems-types/${page.id}`}>
                                                    {page[switcherBackEndLanguage('category')]}
                                                </NavLink>
                                            </li>
                                        )
                                    })
                                }
                            </ul>
                        </li>
                        <li>
                            <NavLink exact activeClassName='is-active' to={`${baseLocaleUrl}/catalog`}>{t('menu_catalog')}</NavLink>
                        </li>
                        <li>
                            <NavLink exact activeClassName='is-active' to={`${baseLocaleUrl}/systems-types`}>{t('menu_systems-types')}</NavLink>
                        </li>
                        <li>
                            <NavLink activeClassName='is-active' to={`${baseLocaleUrl}/news`}>{t('menu_news')}</NavLink>
                        </li>
                        <li>
                            <NavLink activeClassName='is-active' to={`${baseLocaleUrl}/projects`}>{t('menu_projects')}</NavLink>
                        </li>
                        <li>
                            <NavLink activeClassName='is-active' to={`${baseLocaleUrl}/customers`}>{t('menu_customers')}</NavLink>
                        </li>
                        <li>
                            <NavLink activeClassName='is-active' to={`${baseLocaleUrl}/contact-us`}>{t('menu_contact-us')}</NavLink>
                        </li>
                    </ul>
                    <ul className='top-navbar-language'>
                        <li
                            className={`top-navbar-language__item ${i18n.language === language.ru ? 'top-navbar-language__item_active' : ''}`}
                            onClick={() => changeLanguage(language.ru)}
                        >
                            Рус
                        </li>
                        <li
                            className={`top-navbar-language__item ${i18n.language === language.ua ? 'top-navbar-language__item_active' : ''}`}
                            onClick={() => changeLanguage(language.ua)}
                        >
                            Укр
                        </li>
                    </ul>
                </div>
            </div>
        </header>
    );
};

export default withRouter(Header);
