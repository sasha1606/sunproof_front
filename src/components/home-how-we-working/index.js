import React from 'react';
import {useTranslation} from "react-i18next";
import {Col, Container, Row} from "react-bootstrap";

import homeHowWeWorkingImg1 from "../../assets/img/how-we-working/how-we-working-1.png";
import homeHowWeWorkingImg2 from "../../assets/img/how-we-working/how-we-working-2.png";
import homeHowWeWorkingImg3 from "../../assets/img/how-we-working/how-we-working-3.png";
import homeHowWeWorkingImg4 from "../../assets/img/how-we-working/how-we-working-4.png";

import './index.scss';

const HomeHowWeWorking = () => {

    const { t } = useTranslation();

    return (
        <div className='home-how-we-working'>
            <h5 className='home-how-we-working__title'>{t('home_how-work')}</h5>
            <Container>
                <Row>
                    <Col lg={3} md={12}>
                        <div className='home-how-we-working-item__wrap'>
                            <div className='home-how-we-working-item'>
                                <div className='home-how-we-working-item__container'>
                                    <div className='home-how-we-working-item__number'>1</div>
                                    <div className='home-how-we-working-item__line'>
                                        <div className='home-how-we-working-item__content'>
                                            <div className='home-how-we-working-item__title'>
                                                {t('record')}
                                            </div>
                                            <div className='home-how-we-working-item__text'>
                                                {t('home_how-work-1-1')},
                                                <br />
                                                {t('home_how-work-1-2')}
                                                <br />
                                                {t('home_how-work-1-3')}:
                                                <br />
                                                {t('with')} 7:00 {t('to')} 20:00,
                                                <br />
                                                {t('home_how-work-1-4')}.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="hexagon">
                                    <div className="hexagon__inside"></div>
                                    <div className="hexagon__img-wrap">
                                        <img src={homeHowWeWorkingImg1} alt=''/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Col>
                    <Col lg={3} md={12}>
                        <div className='home-how-we-working-item__wrap'>
                            <div className='home-how-we-working-item home-how-we-working-item_revert'>
                                <div className="hexagon">
                                    <div className="hexagon__inside"></div>
                                    <div className="hexagon__img-wrap">
                                        <img src={homeHowWeWorkingImg2} alt=''/>
                                    </div>
                                </div>
                                <div className='home-how-we-working-item__container'>
                                    <div className='home-how-we-working-item__number'>2</div>
                                    <div className='home-how-we-working-item__line'>
                                        <div className='home-how-we-working-item__content'>
                                            <div className='home-how-we-working-item__title'>
                                                {t('home_how-work-2')}
                                            </div>
                                            <div className='home-how-we-working-item__text'>
                                                {t('home_how-work-2-1')}.
                                                <br />
                                                {t('home_how-work-2-2')}
                                                <br />
                                                {t('home_how-work-2-3')}
                                                <br />
                                                {t('home_how-work-2-4')}
                                                <br />
                                                {t('home_how-work-2-5')}.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Col>
                    <Col lg={3} md={12}>
                        <div className='home-how-we-working-item__wrap'>
                            <div className='home-how-we-working-item'>
                                <div className='home-how-we-working-item__container'>
                                    <div className='home-how-we-working-item__number'>3</div>
                                    <div className='home-how-we-working-item__line'>
                                        <div className='home-how-we-working-item__content'>
                                            <div className='home-how-we-working-item__title'>
                                                {t('home_how-work-3')}
                                            </div>
                                            <div className='home-how-we-working-item__text'>
                                                {t('home_how-work-3-1')},
                                                <br />
                                                {t('home_how-work-3-2')}.
                                                <br />
                                                {t('home_how-work-3-3')}:
                                                <br />
                                                - {t('home_how-work-3-4')};
                                                <br />
                                                - {t('home_how-work-3-5')};
                                                <br />
                                                - {t('home_how-work-3-6')}.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="hexagon">
                                    <div className="hexagon__inside"></div>
                                    <div className="hexagon__img-wrap">
                                        <img src={homeHowWeWorkingImg3} alt=''/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Col>
                    <Col lg={3} md={12}>
                        <div className='home-how-we-working-item__wrap'>
                            <div className='home-how-we-working-item home-how-we-working-item_revert'>
                                <div className="hexagon">
                                    <div className="hexagon__inside"></div>
                                    <div className="hexagon__img-wrap">
                                        <img src={homeHowWeWorkingImg4} alt=''/>
                                    </div>
                                </div>
                                <div className='home-how-we-working-item__container'>
                                    <div className='home-how-we-working-item__number'>4</div>
                                    <div className='home-how-we-working-item__line'>
                                        <div className='home-how-we-working-item__content'>
                                            <div className='home-how-we-working-item__title'>
                                                {t('home_how-work-4')}
                                            </div>
                                            <div className='home-how-we-working-item__text'>
                                                - {t('home_how-work-4-1')}
                                                <br />
                                                {t('home_how-work-4-2')}.
                                                <br />
                                                - {t('home_how-work-4-3')}.
                                                <br />
                                                - {t('home_how-work-4-4')}.
                                                <br />
                                                - {t('home_how-work-4-5')}.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
};

export default HomeHowWeWorking;
