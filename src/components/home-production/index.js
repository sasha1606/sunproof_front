import React, {useEffect, useState} from 'react';
import {Link} from "react-router-dom";
import {Col, Container, Row} from "react-bootstrap";
import { useTranslation } from "react-i18next";

import { useDispatch, useSelector } from "react-redux";

// import { pagesInfo } from '../../actions/pages.action';

import moreIcon from "../../assets/img/icon/more.png";

import {baseLocaleUrl} from "../../const/baseLocaleUrl";

import fullImgPath from "../../helpers/fullImgPath";
import switcherBackEndLanguage from "../../helpers/switcherBackEndLanguage";

import './index.scss';

const HomeProduction = () => {
    const pageLimit = 6;
    const [pages, updatePages] = useState([]);

    const dispatch = useDispatch();

    const { t } = useTranslation();

    const pagesStore = useSelector((store) => {
        return store.pagesReducer.pages
    });

    // useEffect(() => {
    //     if(!pagesStore.length) {
    //         dispatch(pagesInfo.getAllPages())
    //     }
    // }, []);

    useEffect(() => {
        if(pagesStore.length && pagesStore.length > pageLimit) {
            const allPages = [...pagesStore];
            allPages.length = pageLimit;
            updatePages(allPages);
        } else if (pagesStore.length) {
            updatePages(pagesStore);
        }
    }, [pagesStore]);

    return (
        <div className='home-production'>
            <h5 className='home-production__title'>{t('company-products')} «Sunproof»</h5>
            <Container fluid={true}>
                <Row>
                    { pages.map((page) => {
                        return (
                            <Col key={page.id} lg={2} md={4} xs={12}>
                                <div className='home-production__item-wrap'>
                                    <Link to={`${baseLocaleUrl}/systems-types/${page.id}`} className='home-production__item'>
                                        <div
                                            className='home-production__img'
                                            style={{backgroundImage: `url(${fullImgPath(page.img_top)})`}}>

                                        </div>
                                        <div className='home-production__content'>
                                            <h6 className='home-production__content-title'>{page[switcherBackEndLanguage('category')]}</h6>
                                            <p
                                                dangerouslySetInnerHTML={{__html: page[switcherBackEndLanguage('text_1')]}}
                                            ></p>
                                            <img className='home-production__more' src={moreIcon} alt='more'/>
                                        </div>
                                    </Link>
                                </div>
                            </Col>
                        )
                    })}

                </Row>
            </Container>
        </div>
    )
};

export default HomeProduction;
