import React, {useState} from 'react';
import {Container, Row, Col} from "react-bootstrap";
import InputMask from 'react-input-mask';
import { useTranslation } from "react-i18next";

import freeCallImg1 from '../../assets/img/free-call_1.png';
import freeCallImg2 from '../../assets/img/free_call_2.png';

import { mapFormErrors } from "../../helpers/mapFormErrors";

import SuccessfullySentModal from "../modal-window/successfully-sent-modal";

import {contactUs} from "../../services/contact-us";

import {formStatus} from "../../const/formStatus";

import './index.scss';

const FreeMeasurement = () => {

    const [name, updateName] = useState('');
    const [phone, updatePhone] = useState('');

    const [formErrors, updateFormErrors] = useState({});

    const [isOpenSuccessfullySentModal, updateIsOpenSuccessfullySentModal] = useState(false);

    const { t } = useTranslation();

    const sendForm = (event) => {
        event.preventDefault();
        contactUs.sendFreeMeasurement(name, phone).then(
            (response) => {
                if (response.data.status === formStatus.error) {
                    const errors = mapFormErrors(response.data.error);
                    updateFormErrors(errors)
                } else {
                    commentsSuccessfullySent();
                }
            },
            (error) => {
                console.error(error);
                // remove
                commentsSuccessfullySent();
            }
        )
    };

    const commentsSuccessfullySent = () => {
        updateName('');
        updatePhone('');

        updateFormErrors({});

        updateIsOpenSuccessfullySentModal(true);
    };

    const closeSuccessfullySentModal = () => {
        updateIsOpenSuccessfullySentModal(false);
    };


    return (
        <React.Fragment>

            <div className='free-measurement'>
                <Container>
                    <h6 className='free-measurement__title'>{t('free-measurement')}</h6>
                    <div className='free-measurement__from-wrap'>
                        <Row>
                            <Col lg={4} xs={1}>
                                <div className='free-measurement__wrap-img-left'>
                                    <img src={freeCallImg2} alt='free-call'/>
                                </div>
                            </Col>
                            <Col lg={4} xs={10}>
                                <form className='form-bg'>
                                    <div className='form-group'>
                                        <label htmlFor='free-measurement-form-name' className=''>{t('form_name')}</label>
                                        <input
                                            placeholder={t('form_name')}
                                            id='free-measurement-form-name'
                                            name='free-measurement-form-name'
                                            value={name}
                                            // required='required'
                                            // minLength={3}
                                            onChange={e => updateName(e.target.value)}
                                            className='form-control'/>
                                        {formErrors.name &&
                                            <span className='form-control-error form-control-error_white'>{formErrors.name}</span>
                                        }
                                    </div>
                                    <div className='form-group'>
                                        <label htmlFor='free-measurement-form-phone' className=''>{t('form_phone')}</label>
                                        <InputMask
                                            id='free-measurement-form-phone'
                                            name='free-measurement-form-phone'
                                            value={phone}
                                            onChange={e => updatePhone(e.target.value)}
                                            className='form-control'
                                            // required='required'
                                            // minLength={3}
                                            alwaysShowMask={true}
                                            mask="+3\8 999 999 99 99"/>
                                        {formErrors.phone &&
                                            <span className='form-control-error form-control-error_white'>{formErrors.phone}</span>
                                        }
                                    </div>
                                </form>
                            </Col>
                            <Col lg={4} xs={1}>
                                <div className='free-measurement__wrap-img-right'>
                                    <img src={freeCallImg1} alt='free-call'/>
                                </div>
                            </Col>
                        </Row>
                    </div>
                    <div className='text-center'>
                        <button className='button' type='submit' onClick={(event) => { sendForm(event) }}>{t('sign-up')}</button>
                    </div>
                </Container>
            </div>
            {isOpenSuccessfullySentModal &&
            <SuccessfullySentModal
                message={t('message-delivered')}
                closeActiveModal={closeSuccessfullySentModal}/>
            }
        </React.Fragment>
    )
};

export default FreeMeasurement
