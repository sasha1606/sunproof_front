import React from "react";
import {useTranslation} from "react-i18next";

import fullImgPath from '../../helpers/fullImgPath';

import './index.scss';
import switcherBackEndLanguage from "../../helpers/switcherBackEndLanguage";

const SingleNews = ({news}) => {
    const showStock = '1';

    const { t } = useTranslation();

    return (
        <div className='single-news'>
            <div className='single-news__header'>
                <span className='single-news__date'>{news.date}</span>
                {news.offer === showStock && <span className='single-news__stock'>{t('action')}</span> }
            </div>
            <div className='single-news__img' style={{backgroundImage: `url(${fullImgPath(news.image)})`}}></div>
            <div className='single-news__title'>{news[switcherBackEndLanguage('title')]}</div>
            <div className='single-news__content'
                 dangerouslySetInnerHTML={{__html: news[switcherBackEndLanguage('description')]}}
            >
            </div>
        </div>
    )
};

export default SingleNews;
