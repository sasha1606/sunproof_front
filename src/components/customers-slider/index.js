import React from 'react';
import Slider from "react-slick";

import arrowSlides from "../../assets/img/icon/arrow_balck.png";

import fullImgPath from "../../helpers/fullImgPath";

import './index.scss';

function NextArrow(props) {
    const {currentSlide, slideCount, className, onClick} = props;

    return <img  className={className} onClick={onClick} src={arrowSlides} alt='nextArrow'/>
}

function PrevArrow(props) {
    const {currentSlide, slideCount, className, onClick} = props;

    return <img  className={className} onClick={onClick} src={arrowSlides} alt='prevArrow'/>
}


const slickSettings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 3,
            }
        },
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 2,
            }
        },
        {
            breakpoint: 500,
            settings: {
                slidesToShow: 1,
            }
        }
    ]
};

const CustomersSlider = ({customers}) => {
    return (
        <div className="customers-slider">
            <Slider className='customers-slider__slider' {...slickSettings}>
                {customers.map( (customer) => {
                    return (
                        <div key={customer.id} className='customers-slider__item'>
                            <img
                                className='customers-slider__item-img'
                                src={fullImgPath(customer.image)}
                                alt='customer'
                            />
                        </div>
                    )
                })}
            </Slider>
        </div>
    );
};

export default CustomersSlider;
