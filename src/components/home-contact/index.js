import React from 'react';
import {useTranslation} from "react-i18next";
import {Col, Container, Row} from "react-bootstrap";

import timeIcon from "../../assets/img/icon/time_black.svg";
import callIcon from "../../assets/img/icon/call_black.svg";
import emailIcon from "../../assets/img/icon/email_black.svg";

import './index.scss';

const HomeContact = () => {

    const { t } = useTranslation();

    return (
        <div className='home-contact'>
            <Container>
                <Row>
                    <Col lg={4} md={12}>
                        <div className='home-contact__item'>
                            <img className='home-contact__icon' src={timeIcon} alt='time'/>
                            <h6 className='home-contact__title'>{t('work-schedule')}</h6>
                            <div className='home-contact__info'>{t('work-schedule-days')} 08:00-20:00</div>
                        </div>
                    </Col>
                    <Col lg={4} md={12}>
                        <div className='home-contact__item'>
                            <img className='home-contact__icon' src={callIcon} alt='time'/>
                            <h6 className='home-contact__title'>{t('contact-numbers')}</h6>
                            <div className='home-contact__info'>
                                <a href='tel:+380938990770' className='home-contact__info-link'>+380938990770</a>
                                <a href='tel:+380688990770' className='home-contact__info-link'>+380688990770</a>
                                <a href='tel:+380958990770' className='home-contact__info-link'>+380958990770</a>
                            </div>
                        </div>
                    </Col>
                    <Col lg={4} md={12}>
                        <a href='mailto:Sunproofua@gmail.com' className='home-contact__item home-contact__item_link'>
                            <img className='home-contact__icon' src={emailIcon} alt='time'/>
                            <h6 className='home-contact__title'>Email</h6>
                            <div className='home-contact__info'>
                                <span className='home-contact__info-link'>Sunproofua@gmail.com</span>
                            </div>
                        </a>
                    </Col>
                </Row>
            </Container>
        </div>
    );
};

export default HomeContact;
