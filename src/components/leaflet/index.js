import { Map as LeafletMap, Marker, Popup, TileLayer } from "react-leaflet";
import React from "react";
import L from "leaflet";

import icon from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';

import "leaflet/dist/leaflet.css";
import "./index.scss"

const leafletPointSettings = {
    lat: 50.468872,
    lng: 30.624308,
    zoom: 13,
};

const Leaflet = () => {

    const leafletPosition = [leafletPointSettings.lat, leafletPointSettings.lng];

    const defaultLeafletIcon = L.icon({
        iconUrl: icon,
        shadowUrl: iconShadow
    });

    L.Marker.prototype.options.icon = defaultLeafletIcon;

    return (
        <LeafletMap
            center={leafletPosition}
            zoom={leafletPointSettings.zoom}
            attributionControl={true}
            zoomControl={true}
            doubleClickZoom={true}
            scrollWheelZoom={true}
            dragging={true}
            animate={true}
            easeLinearity={0.35}
        >
            <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <Marker position={leafletPosition}>
                <Popup>
                    Sunproof
                </Popup>
            </Marker>
        </LeafletMap>
    )
}

export default Leaflet;
