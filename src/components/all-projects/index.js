import React, {useState, useEffect} from 'react';
import {Container, Row, Col} from "react-bootstrap";
import {useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";

import {projectsInfo} from '../../actions/projects.action';

import {paginationLimitProjects} from "../../const/pagination-limit";

import SliderProductModal from "../modal-window/slider-product-modal";
import AllProjectsCategory from "./all-projects-category";
import Pagination from "../pagination";

import fullImgPath from "../../helpers/fullImgPath";

import './index.scss'
import switcherBackEndLanguage from "../../helpers/switcherBackEndLanguage";

const AllProjects = ({withoutPagination, showAllProjectsBtn = true, projectsLimit = paginationLimitProjects}) => {

    const dispatch = useDispatch();
    const history = useHistory();

    const [isOpenSliderProductModal, updateIsOpenSliderProductModal] = useState(false);
    const [clickedProject, updateClickedProject] = useState(null);

    const [locationQueryCategoryId, updateLocationQueryCategoryId] = useState(false);

    const [selectedCategoryId, updateSelectedCategoryId] = useState(null);
    const [currentPage, updateCurrentPage] = useState(1);

    const [isFirstPage, updateIsFirstPage] = useState(false);
    const [isLastPage, updateIsLastPage] = useState(false);

    const projectsStore = useSelector((store) => {
        return {
            allProject: store.projectsReducer.projects.data,
            projectPagination: store.projectsReducer.projects.pagination,
            projectsCategory: store.projectsReducer.projectsCategory,
        }
    });

    useEffect(() => {
        if (!projectsStore.projectsCategory.length) {
            dispatch(projectsInfo.getProjectsCategory())
        }
    }, []);


    useEffect(() => {

        let categoryId = selectedCategoryId;

        const locationQuery = history.location.query;

        if (!locationQueryCategoryId && locationQuery && locationQuery.projectsCategoryId) {
            const projectsCategoryId = String(locationQuery.projectsCategoryId);

            categoryId = projectsCategoryId;
            updateLocationQueryCategoryId(projectsCategoryId)
        }

        dispatch(projectsInfo.getProjects(currentPage, projectsLimit, categoryId))
    }, [selectedCategoryId, currentPage]);

    useEffect(() => {
        updateLastOrFirstPage();
    }, [projectsStore.allProject])

    const closeSliderProductModal = () => {
        updateClickedProject(null);
        toggleSliderProductModal(false)
    };

    const toggleSliderProductModal = (isOpenModal) => {
        updateIsOpenSliderProductModal(isOpenModal)
    };

    const openSliderProductModal = (project) => {
        updateClickedProject(project);
        toggleSliderProductModal(true);
    };

    const categoryFilter = (categoryId) => {
        updateSelectedCategoryId(categoryId);
        updateCurrentPage(1);
    };

    const handlePageClick = (page) => {
        updateCurrentPage(page.selected + 1);
    };

    const nextPage = () => {
        updateCurrentPage(currentPage + 1);
    }

    const prevPage = () => {
        updateCurrentPage(currentPage - 1);
    }

    const updateLastOrFirstPage = () => {
        updateIsLastPage(false);
        updateIsFirstPage(false);

        if ( projectsStore.projectPagination.pages === currentPage || withoutPagination ) {
            updateIsLastPage(true)
        }

        if (currentPage === 1 || withoutPagination) {
            updateIsFirstPage(true)
        }
    };

    return (
        <React.Fragment>
            <div className='all-projects'>
                <div className='all-projects__nav'>
                    <AllProjectsCategory
                        showAllProjectsBtn={showAllProjectsBtn}
                        projectsCategory={projectsStore.projectsCategory}
                        locationQueryCategoryId={locationQueryCategoryId}
                        categoryFilter={categoryFilter}
                    />
                </div>
                <div className='all-projects__content'>
                    <Container fluid={true}>
                        <Row>
                            {projectsStore.allProject.map((project) => {
                                return (
                                    <Col key={project.id} lg={4} md={6} xs={12}>
                                        <div onClick={() => openSliderProductModal(project)}
                                             className='all-projects__item'
                                             style={{backgroundImage: `url(${fullImgPath(project.image)})`}}>
                                            <div className='all-projects__item-description'>{project[switcherBackEndLanguage('name')]}</div>
                                        </div>
                                    </Col>
                                )
                            })}
                        </Row>
                    </Container>
                </div>
            </div>
            {!withoutPagination && projectsStore.projectPagination.pages > 1 &&
                <Pagination
                    currentPage={currentPage}
                    pageCount={projectsStore.projectPagination.pages}
                    handlePageClick={handlePageClick}/>
            }
            {isOpenSliderProductModal &&
                <SliderProductModal
                    products={projectsStore.allProject}
                    selectedProducts={clickedProject}

                    uploadPrevProduct={prevPage}
                    uploadNextProduct={nextPage}

                    isLastPage={isLastPage}
                    isFirstPage={isFirstPage}

                    closeActiveModal={closeSliderProductModal}/>
            }
        </React.Fragment>
    )
};

export default AllProjects;
