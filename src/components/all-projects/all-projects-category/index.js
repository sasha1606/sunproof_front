import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from "react-bootstrap";
import {useTranslation} from "react-i18next";

import switcherBackEndLanguage from "../../../helpers/switcherBackEndLanguage";

import './index.scss'

const AllProjectsCategory = ({projectsCategory, locationQueryCategoryId, categoryFilter, showAllProjectsBtn = true}) => {

    const [ idActiveCategory, updateIdActiveCategory] = useState(null);

    const { t } = useTranslation();

    useEffect(() => {
        if (locationQueryCategoryId) {
            updateIdActiveCategory(locationQueryCategoryId)
        }

    }, [locationQueryCategoryId]);

    const filterByCategory = (categoryId) => {
        updateIdActiveCategory(categoryId);
        categoryFilter(categoryId)
    };

    return (
        <Container fluid={true}>
            <Row>
                { showAllProjectsBtn ?
                    <Col lg={2} md={4} xs={12}>
                        <button
                            onClick={() => filterByCategory()}
                            className={`all-projects-category__nav-btn ${!idActiveCategory ? 'all-projects-category__nav-btn_active' : ''}`}
                            type='button'>
                            {t('all-projects')}
                        </button>
                    </Col>
                    :
                    null
                }

                {projectsCategory.map(category => {
                   return (
                        <Col key={category.id} lg={2} md={4} xs={14}>
                            <button
                                onClick={() => filterByCategory(category.id)}
                                className={`all-projects-category__nav-btn ${idActiveCategory === category.id ? 'all-projects-category__nav-btn_active' : ''}`}
                                type='button'>
                                {category[switcherBackEndLanguage('name')]}
                                </button>
                        </Col>
                   )
                })}
            </Row>
        </Container>

    )
}

export default AllProjectsCategory;
