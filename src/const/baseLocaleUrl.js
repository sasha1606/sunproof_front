import i18n from '../i18n';
import { language } from './language'

export const baseLocaleUrl = i18n.language === language.ua ? '' : '/'+i18n.language;