export const paginationLimitProjects = 9;
export const paginationLimitNews = 3;
export const paginationLimitSystemsTypes = 18;
export const paginationLimitCatalog = 18;
