export const baseUrl = 'https://sunadmin.kiev.ua';
export const baseImgUrl = 'https://sunadmin.kiev.ua';

export const apiPath = {
    news: 'news',
    comments: 'comments',
    mainSlider: 'main-slider',
    projects: 'projects',
    projectsCategory: 'projects-category',
    systems: 'systems',
    systemCategory: 'system-category',
    catalog: 'catalog',
    catalogCategory: 'catalog-category',
    customers: 'clients-img',
    pages: 'pages',
    settings: 'settings',

    sendComments: 'post-comment',
    sendContactUs: 'post-contact',
    sendClient: 'post-client',
    sendSubscribe: 'post-subscribe',
};



