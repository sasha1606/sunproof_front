const customersActionType = {
    customers_success: 'customers_success',
    customers_failed: 'customers_failed',
};

export default customersActionType;
