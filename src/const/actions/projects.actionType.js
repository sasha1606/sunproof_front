const projectsActionType = {
    projects_success: 'projects_success',
    projects_failed: 'projects_failed',

    projects_category_success: 'projects_category_success',
    projects_category_failed: 'projects_category_failed',
};

export default projectsActionType;
