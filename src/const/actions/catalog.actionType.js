const catalogActionType = {
    catalog_success: 'catalog_success',
    catalog_failed: 'catalog_failed',

    catalog_category_success: 'catalog_category_success',
    catalog_category_failed: 'catalog_category_failed',
};

export default catalogActionType;
