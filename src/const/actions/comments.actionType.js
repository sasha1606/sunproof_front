const commentsActionType = {
    comments_success: 'comments_success',
    comments_failed: 'comments_failed',
};

export default commentsActionType;
