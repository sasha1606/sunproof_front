const pageInfoActionType = {
    main_slider_success: 'main_slider_success',
    main_slider_failed: 'main_slider_failed',
};

export default pageInfoActionType;
