const systemsTypesActionType = {
    systems_type_success: 'systems_type_success',
    systems_type_failed: 'systems_type_failed',

    systems_type_category_success: 'systems_type_category_success',
    systems_type_category_failed: 'systems_type_category_failed',
};

export default systemsTypesActionType;
