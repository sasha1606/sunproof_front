const pagesActionType = {
    pages_success: 'pages_success',
    pages_failed: 'pages_failed',

    single_page_success: 'single_page_success',
    single_page_failed: 'single_page_failed',
};

export default pagesActionType;
