const testActionType = {
    add_test_data: 'add_test_data',
    remove_test_data: 'remove_test_data',
};

export default testActionType;
