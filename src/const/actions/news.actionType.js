const newsActionType = {
    news_success: 'news_success',
    news_failed: 'news_failed',
};

export default newsActionType;
