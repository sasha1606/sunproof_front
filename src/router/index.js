import React from 'react';
import {Switch, Route, Redirect} from "react-router-dom";

import Home from '../pages/home'
import {ContactUs} from '../pages/contact-us'
import Catalog from "../pages/catalog";
import SystemsTypesSingle from "../pages/systems-types-single";
import SystemsTypes from "../pages/systems-types";
import Customers from "../pages/customers";
import News from "../pages/news";
import Projects from "../pages/projects";

import {baseLocaleUrl} from "../const/baseLocaleUrl";

const baseRouteUrl = "/:locale(ru)?";

const Navigation = function () {
    return (
        <Switch>
            <Route exact path={`${baseRouteUrl}/`} component={Home} />
            <Route exact path={`${baseRouteUrl}/contact-us`} component={ContactUs} />
            <Route exact path={`${baseRouteUrl}/catalog`} component={Catalog} />
            <Route exact path={`${baseRouteUrl}/systems-types/:categoryId`} component={SystemsTypesSingle} />
            <Route exact path={`${baseRouteUrl}/systems-types`} component={SystemsTypes} />
            <Route exact path={`${baseRouteUrl}/customers`} component={Customers} />
            <Route exact path={`${baseRouteUrl}/news`} component={News} />
            <Route exact path={`${baseRouteUrl}/projects`} component={Projects} />

            <Redirect from='*' to={`${baseLocaleUrl}/`}/>
        </Switch>
    )
};

export default Navigation;
