import catalogActionType from "../const/actions/catalog.actionType";

const initialState = {
    catalog: {
        data: [],
        pagination: {}
    },
    catalogCategory: [],
};

const catalogReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case catalogActionType.catalog_success:
            return {
                ...state,
                catalog: action.payload,
            };
            break;
        case catalogActionType.catalog_failed:
            return {
                ...state,
                catalog: initialState.catalog,
            };
            break;
        case catalogActionType.catalog_category_success:
            return {
                ...state,
                catalogCategory: action.payload,
            };
            break;
        case catalogActionType.catalog_category_failed:
            return {
                ...state,
                catalogCategory: [],
            };
            break;

        default:
            return state;
    }
};

export default catalogReducer;
