import testActionType from "../const/actions/test.actionType";

const initialState = [];

const testReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case testActionType.add_test_data:
            return [
                ...state,
                ...action.payload,
            ];
            break;
        case testActionType.remove_test_data:
            return [];
            break;

        default:
            return state;
    }
};

export default testReducer;
