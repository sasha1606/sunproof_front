import newsActionType from "../const/actions/news.actionType";

const initialState = {
    news: {
        data: [],
        pagination: {}
    },
};

const newsReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case newsActionType.news_success:
            return {
                ...state,
                news: action.payload,
            };
            break;
        case newsActionType.news_failed:
            return initialState;
            break;

        default:
            return state;
    }
};

export default newsReducer;
