import projectsActionType from "../const/actions/projects.actionType";

const initialState = {
    projects: {
        data: [],
        pagination: {}
    },
    projectsCategory: [],
};

const projectsReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case projectsActionType.projects_success:
            return {
                ...state,
                projects: action.payload,
            };
            break;
        case projectsActionType.projects_failed:
            return {
                ...state,
                projects: initialState.projects,
            };
            break;
        case projectsActionType.projects_category_success:
            return {
                ...state,
                projectsCategory: action.payload,
            };
            break;
        case projectsActionType.projects_category_failed:
            return {
                ...state,
                projectsCategory: [],
            };
            break;

        default:
            return state;
    }
};

export default projectsReducer;
