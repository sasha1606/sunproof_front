import pageInfoActionType from "../const/actions/pageInfo.actionType";

const initialState = {
    mainSlider: [],
};

const pageInfoReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case pageInfoActionType.main_slider_success:
            return {
                ...state,
                mainSlider: action.payload,
            };
            break;
        case pageInfoActionType.main_slider_failed:
            return initialState;
            break;

        default:
            return state;
    }
};

export default pageInfoReducer;
