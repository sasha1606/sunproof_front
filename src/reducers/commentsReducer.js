import commentsActionType from "../const/actions/comments.actionType";

const initialState = {
    comments: {
        data: [],
        pagination: {}
    },
};

const commentsReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case commentsActionType.comments_success:
            return {
                ...state,
                comments: action.payload,
            };
            break;
        case commentsActionType.comments_failed:
            return initialState;
            break;

        default:
            return state;
    }
};

export default commentsReducer;
