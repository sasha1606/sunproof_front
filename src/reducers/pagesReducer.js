import pagesActionType from "../const/actions/pages.actionType";

const initialState = {
    pages: [],
    singlePage: [],
};

const pagesReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case pagesActionType.pages_success:
            return {
                ...state,
                pages: action.payload,
            };
            break;
        case pagesActionType.pages_failed:
            return {
                ...state,
                pages: [],
            };
            break;
        case pagesActionType.single_page_success:
            return {
                ...state,
                singlePage: action.payload,
            };
            break;
        case pagesActionType.single_page_failed:
            return {
                ...state,
                singlePage: [],
            };
            break;

        default:
            return state;
    }
};

export default pagesReducer;
