import systemsTypesActionType from "../const/actions/systems-types.actionType";

const initialState = {
    systemsTypes: {
        data: [],
        pagination: {}
    },
    systemsTypesCategory: [],
};

const systemsTypesReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case systemsTypesActionType.systems_type_success:
            return {
                ...state,
                systemsTypes: action.payload,
            };
            break;
        case systemsTypesActionType.systems_type_failed:
            return {
                ...state,
                systemsTypes: initialState.systemsTypes,
            };
            break;
        case systemsTypesActionType.systems_type_category_success:
            return {
                ...state,
                systemsTypesCategory: action.payload,
            };
            break;
        case systemsTypesActionType.systems_type_category_failed:
            return {
                ...state,
                systemsTypesCategory: [],
            };
            break;

        default:
            return state;
    }
};

export default systemsTypesReducer;
