import customersActionType from "../const/actions/customers.actionType";

const initialState = {
    customers: [],
};

const customersReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case customersActionType.customers_success:
            return {
                ...state,
                customers: action.payload,
            };
            break;
        case customersActionType.customers_failed:
            return initialState;
            break;

        default:
            return state;
    }
};

export default customersReducer;
