import {combineReducers} from 'redux';

import testReducer from './testReducer';
import newsReducer from "./newsReducer";
import commentsReducer from "./commentsReducer";
import pageInfoReducer from "./pageInfoReducer";
import projectsReducer from "./projectsReducer";
import systemsTypesReducer from "./systemsTypesReducer";
import catalogReducer from "./catalogReducer";
import customersReducer from "./customersReducer";
import pagesReducer from "./pagesReducer";

const rootReducers = combineReducers(
    {
        testReducer,
        newsReducer,
        commentsReducer,
        pageInfoReducer,
        projectsReducer,
        systemsTypesReducer,
        catalogReducer,
        customersReducer,
        pagesReducer
    }
);

export default rootReducers;
