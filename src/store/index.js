import { createStore, applyMiddleware } from 'redux'
import ReduxThunk from 'redux-thunk';
import rootReducers from "../reducers/rootReducers";
// import { composeWithDevTools } from 'redux-devtools-extension';
// composeWithDevTools(applyMiddleware(ReduxThunk))

export default function configureStore(initialState) {
    return createStore(
        rootReducers,
        initialState,
        applyMiddleware(ReduxThunk)
    );
}
