import newsActionType from "../const/actions/news.actionType";
import getNews from '../services/news';

export function getAllNews(page, pageLimit) {

    return dispatch => {
        getNews(page, pageLimit)
            .then(
                state => {
                    dispatch(success(state.data));
                },
                error => {
                    dispatch(failed(error));
                }
            );

        const success = (state) => {
            return {type: newsActionType.news_success, payload: state}
        };

        const failed = (error) => {
            return {type: newsActionType.news_failed, error}
        };
    }
}
