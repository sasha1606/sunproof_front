import catalogActionType from "../const/actions/catalog.actionType";
import { catalog } from '../services/catalog';

const getCatalog = (page, pageLimit, categoryId) => {

    return dispatch => {
        catalog.getCatalog(page, pageLimit, categoryId)
            .then(
                state => {
                    dispatch(success(state.data));
                },
                error => {
                    dispatch(failed(error));
                }
            );

        const success = (state) => {
            return {type: catalogActionType.catalog_success, payload: state}
        };

        const failed = (error) => {
            return {type: catalogActionType.catalog_failed, error}
        };
    }
};

const getCatalogCategory = () => {

    return dispatch => {
        catalog.getCatalogCategory()
            .then(
                state => {
                    dispatch(success(state.data));
                },
                error => {
                    dispatch(failed(error));
                }
            );

        const success = (state) => {
            return {type: catalogActionType.catalog_category_success, payload: state}
        };

        const failed = (error) => {
            return {type: catalogActionType.catalog_category_failed, error}
        };
    }
};

export const catalogInfo = {getCatalog, getCatalogCategory};
