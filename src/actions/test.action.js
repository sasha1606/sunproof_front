import testActionType from "../const/actions/test.actionType";
import getTestData from '../services/test';

export function getAllTestData() {

    return dispatch => {
        getTestData()
            .then(
                state => {
                    dispatch(success(state));
                },
                error => {
                    dispatch(failed(error));
                }
            );

        const success = (state) => {
            return {type: testActionType.add_test_data, payload: state}
        };

        const failed = (error) => {
            return {type: testActionType.remove_test_data, error}
        };
    }
}
