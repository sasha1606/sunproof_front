import customersActionType from "../const/actions/customers.actionType";
import getCustomers from '../services/customers';

export function getAllCustomers() {

    return dispatch => {
        getCustomers()
            .then(
                state => {
                    dispatch(success(state.data));
                },
                error => {
                    dispatch(failed(error));
                }
            );

        const success = (state) => {
            return {type: customersActionType.customers_success, payload: state}
        };

        const failed = (error) => {
            return {type: customersActionType.customers_failed, error}
        };
    }
}
