import projectsActionType from "../const/actions/projects.actionType";
import { projects } from '../services/projects';

const getProjects = (page, pageLimit, categoryId) => {

    return dispatch => {
        projects.getProjects(page, pageLimit, categoryId)
            .then(
                state => {
                    dispatch(success(state.data));
                },
                error => {
                    dispatch(failed(error));
                }
            );

        const success = (state) => {
            return {type: projectsActionType.projects_success, payload: state}
        };

        const failed = (error) => {
            return {type: projectsActionType.projects_failed, error}
        };
    }
};

const getProjectsCategory = () => {

    return dispatch => {
        projects.getProjectsCategory()
            .then(
                state => {
                    dispatch(success(state.data));
                },
                error => {
                    dispatch(failed(error));
                }
            );

        const success = (state) => {
            return {type: projectsActionType.projects_category_success, payload: state}
        };

        const failed = (error) => {
            return {type: projectsActionType.projects_category_failed, error}
        };
    }
};

export const projectsInfo = {getProjects, getProjectsCategory};
