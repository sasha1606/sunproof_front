import pagesActionType from "../const/actions/pages.actionType";
import { pages } from '../services/pages';

const getAllPages = () => {

    return dispatch => {
        pages.getAllPages()
            .then(
                state => {
                    dispatch(success(state.data));
                },
                error => {
                    dispatch(failed(error));
                }
            );

        const success = (state) => {
            return {type: pagesActionType.pages_success, payload: state}
        };

        const failed = (error) => {
            return {type: pagesActionType.pages_failed, error}
        };
    }
};

const getSinglePage = (pageId) => {

    return dispatch => {
        pages.getSinglePage(pageId)
            .then(
                state => {
                    dispatch(success(state.data));
                },
                error => {
                    dispatch(failed(error));
                }
            );

        const success = (state) => {
            return {type: pagesActionType.single_page_success, payload: state}
        };

        const failed = (error) => {
            return {type: pagesActionType.single_page_failed, error}
        };
    }
};

export const pagesInfo = { getAllPages, getSinglePage };
