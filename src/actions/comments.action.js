import commentsActionType from "../const/actions/comments.actionType";
import { comments } from '../services/comments';

export function getAllComments() {

    return dispatch => {
        comments.getComments()
            .then(
                state => {
                    dispatch(success(state.data));
                },
                error => {
                    dispatch(failed(error));
                }
            );

        const success = (state) => {
            return {type: commentsActionType.comments_success, payload: state}
        };

        const failed = (error) => {
            return {type: commentsActionType.comments_failed, error}
        };
    }
}
