import systemsTypesActionType from "../const/actions/systems-types.actionType";
import { systemsTypes } from '../services/systems-types';

const getSystemsTypes = (page, pageLimit, categoryId) => {

    return dispatch => {
        systemsTypes.getSystemsTypes(page, pageLimit, categoryId)
            .then(
                state => {
                    dispatch(success(state.data));
                },
                error => {
                    dispatch(failed(error));
                }
            );

        const success = (state) => {
            return {type: systemsTypesActionType.systems_type_success, payload: state}
        };

        const failed = (error) => {
            return {type: systemsTypesActionType.systems_type_failed, error}
        };
    }
};

const getSystemsTypesCategory = () => {

    return dispatch => {
        systemsTypes.getSystemsTypesCategory()
            .then(
                state => {
                    dispatch(success(state.data));
                },
                error => {
                    dispatch(failed(error));
                }
            );

        const success = (state) => {
            return {type: systemsTypesActionType.systems_type_category_success, payload: state}
        };

        const failed = (error) => {
            return {type: systemsTypesActionType.systems_type_category_failed, error}
        };
    }
};

export const systemsTypesInfo = {getSystemsTypes, getSystemsTypesCategory};
