import pageInfoActionType from "../const/actions/pageInfo.actionType";
import { pageInfo } from '../services/pageInfo';

const getMainSlider = () => {

    return dispatch => {
        pageInfo.getMainSliderInfo()
            .then(
                state => {
                    dispatch(success(state.data));
                },
                error => {
                    dispatch(failed(error));
                }
            );

        const success = (state) => {
            return {type: pageInfoActionType.main_slider_success, payload: state}
        };

        const failed = (error) => {
            return {type: pageInfoActionType.main_slider_failed, error}
        };
    }
};

export const mainPgeInfo = {getMainSlider};
