import {language} from "../const/language";

export const selectedLanguageInUrl = () => {
    const arrayPathname = window.location.pathname.split('/');
    const isLangRu = arrayPathname.includes('ru');

    return isLangRu ? language.ru : language.ua;
}