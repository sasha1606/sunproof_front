import {baseImgUrl} from "../const/api";

const fullImgPath = (path) => {
    if (path) {
        return `${baseImgUrl}${path}`;
    }
    return '/';

};

export default fullImgPath;
