import i18n from "../i18n";

const switcherBackEndLanguage = (variable, lang) => {

    let currentLang = lang;

    if (!currentLang) {
        currentLang = i18n.language;
    }

    if (variable) {
        return `${variable}_${currentLang}`;
    }
    return '';
};

export default switcherBackEndLanguage;
