import i18n from "../i18n";

export const mapFormErrors = (errors) => {
    const mapErrors = {};
    Object.entries(errors).forEach(([name, value]) => {
        const langError = JSON.parse(value);
        mapErrors[name] = langError[i18n.language];
    });

    return mapErrors;
};